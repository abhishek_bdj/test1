package LaunchpadObjects;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import Objects.CommonOperations;

public class Users_And_Groups_Objects {
	
	WebDriver driver;
	CommonOperations co;
	boolean verify = false; 
	public WebDriverWait wait;

	public Users_And_Groups_Objects (WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
//User Page
	@FindBy(how = How.XPATH, using = "//ul[@class='nav nav-tabs']/li/a") 
	public List <WebElement> UserPageTopTabs;
	
	public List <WebElement> getUserPageTopTabs() {
		return UserPageTopTabs;
	}
	
	@FindBy(how = How.XPATH, using = "//h3[contains(text(),'User')]") 
	public WebElement UserPageTitle;
	
	public WebElement getUUserPageTitle() {
		return UserPageTitle;
	}
	
	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Add New User')]") 
	public WebElement BtnAddNewUser;
	
	public WebElement getBtnAddNewUser() {
		return BtnAddNewUser;
	}
	
	@FindBy(how = How.XPATH, using = "//*[@id='usermanager_filter']/label/input") 
	public WebElement TxtSearchUser;
	
	public WebElement getTxtSearchUser() {
		return TxtSearchUser;
	}
	
	@FindBy(how = How.XPATH, using = "//*[@class='user_delete btn btn-second']/i") 
	public WebElement BtnDeleteUser;
	
	public WebElement getBtnDeleteUser() {
		return BtnDeleteUser;
	}
	
	@FindBy(how = How.ID, using = "delete_user") 
	public WebElement BtnConfirmDeleteUser;
	
	public WebElement getBtnConfirmDeleteUser() {
		return BtnConfirmDeleteUser;
	}
	
//User page -> Add new User Fields
	@FindBy(how = How.ID, using = "input_user_email") 
	public WebElement TxtUserEmail;
	
	public WebElement getTxtUserEmail() {
		return TxtUserEmail;
	}
	
	@FindBy(how = How.ID, using = "input_register_password") 
	public WebElement TxtUserPass;
	
	public WebElement getTxtUserPass() {
		return TxtUserPass;
	}
	
	@FindBy(how = How.ID, using = "input_register_repassword") 
	public WebElement TxtUserConfirmPass;
	
	public WebElement getTxtUserConfirmPass() {
		return TxtUserConfirmPass;
	}
	
	@FindBy(how = How.ID, using = "input_user_first_name") 
	public WebElement TxtUserFirstName;
	
	public WebElement getTxtUserFirstName() {
		return TxtUserFirstName;
	}
	
	@FindBy(how = How.ID, using = "input_user_last_name") 
	public WebElement TxtUserLastname;
	
	public WebElement getTxtUserLastname() {
		return TxtUserLastname;
	}
	
	@FindBy(how = How.ID, using = "input_0") 
	public WebElement BtnCreateUser_Save;
	
	public WebElement getBtnCreateUser_Save() {
		return BtnCreateUser_Save;
	}
	
	@FindBy(how = How.NAME, using = "save") 
	public WebElement BtnSaveChanges;
	
	public WebElement getBtnSaveChanges() {
		return BtnSaveChanges;
	}
	
	@FindBy(how = How.XPATH, using = "//*[@id='usermanager']/tbody/tr/td[3]") 
	public WebElement TxtGridRow1UserEmail;
	
	public WebElement getTxtGridRow1Email() {
		return TxtGridRow1UserEmail;
	}
	
	
//Groups
	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Add New Group')]") 
	public WebElement BtnAddNewGroup;
	
	public WebElement getBtnAddNewGroup() {
		return BtnAddNewGroup;
	}
	
	@FindBy(how = How.XPATH, using = "//*[@class='group_delete btn btn-second']/i") 
	public WebElement BtnDeleteGroup;
	
	public WebElement getBtnDeleteGroup() {
		return BtnDeleteGroup;
	}
	
	@FindBy(how = How.XPATH, using = "//*[@id='delete_group']") 
	public WebElement BtnConfirmDeleteGroup;
	
	public WebElement getBtnConfirmDeleteGroup() {
		return BtnConfirmDeleteGroup;
	}
	
	@FindBy(how = How.XPATH, using = "//*[@id='groupmanager_filter']/label/input") 
	public WebElement TxtSearchGroup;
	
	public WebElement getTxtSearchGroup() {
		return TxtSearchGroup;
	}
	
	@FindBy(how = How.ID, using = "input_group_name") 
	public WebElement TxtGroupName;
	
	public WebElement getTxtGroupName() {
		return TxtGroupName;
	}
	
	@FindBy(how = How.XPATH, using = "//button[contains(text(),'Create group')]") 
	public WebElement BtnCreateGroup_Save;
	
	public WebElement getBtnCreateGroup_Save() {
		return BtnCreateGroup_Save;
	}
	
	@FindBy(how = How.XPATH, using = "//*[@class='btn red']") 
	public WebElement BtnTools;
	
	public WebElement getBtnTools() {
		return BtnTools;
	}
	
	@FindBy(how = How.XPATH, using = "//*[@id='send-reset-password']/parent::li/parent::ul/li/a") 
	public List <WebElement> BtnToolsOptionList;
	
	public List <WebElement> getBtnToolsOptionList() {
		return BtnToolsOptionList;
	}
	
	@FindBy(how = How.XPATH, using = "//*[@id='usermanager_filter']/label/input") 
	public WebElement TxtAddUsersInGroupSearchBar;
	
	public WebElement getTxtAddUsersInGroupSearchBar() {
		return TxtAddUsersInGroupSearchBar;
	}
	
	@FindBy(how = How.XPATH, using = "//*[@id='usermanager']/tbody/tr/td/div/span/input") 
	public List <WebElement> CbSelectUserRow;
	
	public List <WebElement> getCbSelectUserRow() {
		return CbSelectUserRow;
	}
	
	@FindBy(how = How.XPATH, using = "//*[@class='user_accept btn blue']") 
	public WebElement BtnAccept;
	
	public WebElement getBtnAccept() {
		return BtnAccept;
	}
	
	@FindBy(how = How.XPATH, using = "//*[@id='groupmanager']/tbody/tr/td[2]") 
	public WebElement TxtGridRow1GroupName;
	
	public WebElement getTxtGridRow1GroupName() {
		return TxtGridRow1GroupName;
	}
	
	@FindBy(how = How.XPATH, using = "//*[contains(text(),'No matching records found')]") 
	WebElement LblGridNoRecordsFound;

	public WebElement getLblGridNoRecordsFound() {
		return LblGridNoRecordsFound;

	}
}

