package LaunchpadObjects;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import Objects.CommonOperations;

public class FrontendWindow_Objects {
	
	public WebDriver driver;
	boolean verify = false;
	public WebElement elememt = null;
	public int SavedTemplateCount = 0;

	public FrontendWindow_Objects(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.XPATH, using = "//*[contains(text(),'Frontend')]")  
	WebElement TitleFrontend;
	
	public WebElement getTitleFrontend() {
		return TitleFrontend;
	}

	@FindBy(xpath = "//*[@id='navigation']/div/div[2]/ul/li/a") 
	List <WebElement> BtnTopMenuList;
	
	public List <WebElement> getBtnTopMenuList() {
		return BtnTopMenuList;
	}
	
	@FindBy(xpath = "//*[@class='row media-items-container']/div/div/a") 
	List <WebElement> GalleryEntryList;
	
	public List <WebElement> getGalleryEntryList() {
		return GalleryEntryList;
	}
	
	@FindBy(xpath = "//*[@class='row media-items-container']/div/div/h4") 
	List <WebElement> GalleryEntryNameList;
	
	public List <WebElement> getGalleryEntryNameList() {
		return GalleryEntryNameList;
	}
	
	
	
	
	
	
}
