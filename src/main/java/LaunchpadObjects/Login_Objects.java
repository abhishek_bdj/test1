package LaunchpadObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class Login_Objects {
	
public WebDriver driver;
	
	public Login_Objects(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.ID, using = "input_login_email") 
	WebElement TxtUsername;
	
	public WebElement getTxtUsername() {
		return TxtUsername;
	}
	
	public void setTxtUsername(WebElement TxtUsername) {
		this.TxtUsername = TxtUsername;
	}
	
	@FindBy(how = How.ID, using = "input_login_password") 
	WebElement TxtPassword;
	
	public WebElement getTxtPassword() {
		return TxtPassword;
	}
	
	public void setTxtPassword(WebElement TxtPassword) {
		this.TxtPassword = TxtPassword;
	}
	
	@FindBy(how = How.NAME, using = "continue") 
	WebElement BtnLogin;
	
	public WebElement getBtnLogin() {
		return BtnLogin;
	}

}
