package LaunchpadObjects;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;


public class BaseClass {

	public WebDriver driver;
	public WebDriverWait wait30;
	
	//@BeforeTest
	public WebDriver setup() {
		//System.setProperty("webdriver.chrome.driver", "D:\\AutomationDrivers\\ChromeDriver\\94.0.4606.61\\chromedriver.exe");
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		
		/*
		 * driver.get("https://manager.uat.launchpad6.com/login?return=/mysite"); wait30
		 * = new WebDriverWait(driver, 30);
		 */
		return driver;
	}
	
	
	
	//@AfterTest
	public void tearDown() {
		driver.quit();
		driver.close();
	}
	
	
	

}
