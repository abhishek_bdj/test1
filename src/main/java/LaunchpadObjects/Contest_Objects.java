package LaunchpadObjects;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import Objects.CommonOperations;

public class Contest_Objects {

	public WebDriver driver;
	boolean verify = false;
	public WebElement elememt = null;
	public CommonOperations co;
	public int SavedTemplateCount = 0;

	public Contest_Objects(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.XPATH, using = "//*[@id='toast-container']/div/div[1]")  
	public WebElement LabelToastMessageTitle;

	@FindBy(xpath = "//*[contains(text(),'Contest')][@class='page-title']") 
	WebElement TitleContest;

	public WebElement getContestTilte() {
		return TitleContest;
	}

	@FindBy(how = How.XPATH, using = "//button[contains(text(),'Create Contest')]") 
	WebElement BtnCreateNewContest;

	public WebElement getBtnCreateNewContest() {
		return BtnCreateNewContest;
	}

	@FindBy(how = How.XPATH, using = "//*[contains(text(),'Create a new contest')]") 
	WebElement WindowCreateNewContest;

	public WebElement getWindowCreateNewContest() {
		return WindowCreateNewContest;
	}

	@FindBy(how = How.XPATH, using = "//label[contains(text(),'Search')]/input") 
	WebElement TxtSearchContest;

	public WebElement getTxtSearchContest() {
		return TxtSearchContest;
	}

	public void setTxtSearchContest(WebElement TxtSearchContest) {
		this.TxtSearchContest = TxtSearchContest;
	}

	//Create a new contest popup
	@FindBy(how = How.ID, using = "input_name") 
	WebElement TxtContestname;

	public WebElement getTxtContestname() {
		return TxtContestname;
	}

	public void setTxtContestname(WebElement TxtContestname) {
		this.TxtContestname = TxtContestname;
	}

	@FindBy(how = How.ID, using = "description") 
	WebElement TxtContestDescription;

	public WebElement getTxtContestDescription() {
		return TxtContestDescription;
	}

	public void setTxtContestDescription(WebElement TxtContestDescription) {
		this.TxtContestDescription = TxtContestDescription;
	}

	@FindBy(how = How.ID, using = "create_new_contest_btn") 
	WebElement BtnSaveContest;

	public WebElement getBtnSaveContest() {
		return BtnSaveContest;
	}

	//Gird
	@FindBy(how = How.XPATH, using = "//*[@id='contestmanager']/tbody/tr[1]/td") 
	List <WebElement> GridRow1;

	public List <WebElement> getGridRow1() {
		return GridRow1;
	}

	@FindBy(how = How.XPATH, using = "//*[@id='contestmanager']/tbody/tr[1]/td[8]/button") 
	List <WebElement> ActionButtons;

	public List <WebElement> getActionButtons() {
		return ActionButtons;
	}
	
	@FindBy(how = How.XPATH, using = "//*[@id='contestmanager']/tbody/tr[1]/td[2]") 
	WebElement GridRow1ContestName;

	public WebElement getGridRow1ContestName() {
		return GridRow1ContestName;
	}
	
	@FindBy(how = How.XPATH, using = "//*[@id='contestmanager']/tbody/tr[1]/td[6]/span") 
	WebElement GridRow1ContestStatus;

	public WebElement getGridRow1ContestStatus() {
		return GridRow1ContestStatus;
	}
	
	@FindBy(how = How.XPATH, using = "//*[contains(text(),'No matching records found')]") 
	WebElement LblGridNoRecordsFound;

	public WebElement getLblGridNoRecordsFound() {
		return LblGridNoRecordsFound;
	}
	
	//Manage Contests
	@FindBy(how = How.XPATH, using = "//*[@class='tabbable tabbable-custom tabbable-full-width']/ul/li/a") 
	public List <WebElement> ManageContestTabNameList;

	public List <WebElement> getManageContestTabNameList() {
		return ManageContestTabNameList;
	}

	//Contest Manage -> Settings -> General
	@FindBy(how = How.ID, using = "input_contest_status") 
	public WebElement DdContestStatus;

	public WebElement getDdContestStatus() {
		return DdContestStatus;
	}

	public void setContestStatus(WebElement ContestStatus) {
		this.DdContestStatus = ContestStatus;
	}

	@FindBy(how = How.NAME, using = "save") 
	public WebElement BtnSettingGeneralSave;

	public WebElement getBtnSettingGeneralSave() {
		return BtnSettingGeneralSave;
	}
	
	//Contest Manage -> Settings
	@FindBy(how = How.XPATH, using = "//*[@class='list-unstyled plugin-nav']/li/a") 
	public List <WebElement> ManageContestLeftMenuNameList;

	public List <WebElement> getManageContestLeftMenuNameList() {
		return ManageContestLeftMenuNameList;
	}
	
	//Contest Manage -> Settings -> Forms
	@FindBy(how = How.ID, using = "add-field") 
	public WebElement BtnAddForm;

	public WebElement getBtnAddForm() {
		return BtnAddForm;
	}

	@FindBy(how = How.XPATH, using = "//*[@id='add-field']/following-sibling::ul/li/a") 
	List <WebElement> BtnAddForm_Type;

	public List <WebElement> getBtnAddForm_Type() {
		return BtnAddForm_Type;
	}

	//Add Vote Entry Form Fields
	@FindBy(how = How.ID, using = "add-field") 
	public WebElement BtnAddFormField;

	public WebElement getBtnAddFormField() {
		return BtnAddFormField;
	}

	@FindBy(how = How.ID, using = "input_label") 
	public WebElement TxtVoteFormLabel;

	public WebElement getTxtVoteFormLabel() {
		return TxtVoteFormLabel;
	}

	public void setTxtVoteFormLabel(WebElement TxtVoteFormLabel) {
		this.TxtVoteFormLabel = TxtVoteFormLabel;
	}

	@FindBy(how = How.ID, using = "input_placeholder") 
	public WebElement TxtVoteFormPlaceholder;

	public WebElement getTxtVoteFormPlaceholder() {
		return TxtVoteFormPlaceholder;
	}

	public void setTxtVoteFormPlaceholder(WebElement TxtVoteFormPlaceholder) {
		this.TxtVoteFormPlaceholder = TxtVoteFormPlaceholder;
	}

	@FindBy(how = How.ID, using = "input_min_length") 
	public WebElement TxtVoteFormMinLengthOfText;

	public WebElement getTxtVoteFormMinLengthOfText() {
		return TxtVoteFormMinLengthOfText;
	}

	public void setTxtVoteFormMinLengthOfText(WebElement TxtVoteFormMinLengthOfText) {
		this.TxtVoteFormMinLengthOfText = TxtVoteFormMinLengthOfText;
	}

	@FindBy(how = How.ID, using = "input_max_length") 
	public WebElement TxtVoteFormMaxLengthOfText;

	public WebElement getTxtVoteFormMaxLengthOfText() {
		return TxtVoteFormMaxLengthOfText;
	}

	public void setTxtVoteFormMaxLengthOfText(WebElement TxtVoteFormMaxLengthOfText) {
		this.TxtVoteFormMaxLengthOfText = TxtVoteFormMaxLengthOfText;
	}

	@FindBy(how = How.NAME, using = "submit") 
	public WebElement BtnFormDetailsSave;

	public WebElement getBtnFormDetailsSave() {
		return BtnFormDetailsSave;
	}

	//Delete Contests
	@FindBy(how = How.XPATH, using = "//*[contains(text(),'Are  you sure?')]") 
	public WebElement DeleteContestConfirmPopup;

	public WebElement getDeleteContestConfirmPopup() {
		return DeleteContestConfirmPopup;
	}

	@FindBy(how = How.ID, using = "confirmDeleteText") 
	public WebElement TxtDeleteContest;

	public WebElement getTxtDeleteContest() {
		return TxtDeleteContest;
	}

	public void setTxtDeleteContest(WebElement TxtDeleteContest) {
		this.TxtDeleteContest = TxtDeleteContest;
	}

	@FindBy(how = How.ID, using = "delete_contest") 
	public WebElement BtnConfirmDeleteContest;

	public WebElement getBtnConfirmDeleteContest() {
		return BtnConfirmDeleteContest;
	}

	//Design Contests
	@FindBy(how = How.XPATH, using = "//*[@class='thumbnail']") 
	public List <WebElement> SavedTemplatesList;

	public List <WebElement> getSavedTemplatesList() {
		return SavedTemplatesList;
	}

	@FindBy(how = How.XPATH, using = "//*[@id='overview']/div[1]/div/div//h3") 
	public List <WebElement> SavedTemplatesTitleList;

	public List <WebElement> getSavedTemplatesTitleList() {
		return SavedTemplatesTitleList;
	}

	@FindBy(how = How.XPATH, using = "//*[@class='checker']/span/input") 
	public List <WebElement> CbDefaultSavedTemplatesList;

	public List <WebElement> getCbDefaultSavedTemplatesList() {
		return CbDefaultSavedTemplatesList;
	}

	public void setCbDefaultSavedTemplatesList(List <WebElement> CbDefaultSavedTemplatesList) {
		this.CbDefaultSavedTemplatesList = CbDefaultSavedTemplatesList;
	}

	@FindBy(how = How.XPATH, using = "//*[@class='btn default delete-btn']/i") 
	public List <WebElement> BtnDeleteSavedTemplatesList;

	public List <WebElement> getBtnDeleteSavedTemplatesList() {
		return BtnDeleteSavedTemplatesList;
	}

	public void setBtnDeleteSavedTemplatesList(List <WebElement> BtnDeleteSavedTemplatesList) {
		this.BtnDeleteSavedTemplatesList = BtnDeleteSavedTemplatesList;
	}

	@FindBy(how = How.XPATH, using = "//*[@id='delete_template']") 
	public WebElement BtnConfirmDeleteTemplates;

	public WebElement getBtnConfirmDeleteTemplates() {
		return BtnConfirmDeleteTemplates;
	}
	/*
	 * @FindBy(how = How.XPATH, using = "//*[@class='bootstrap-switch-label']")
	 * public List <WebElement> BtnActiveSavedTemplatesList;
	 * 
	 * public List <WebElement> getBtnActiveSavedTemplatesList() { return
	 * BtnActiveSavedTemplatesList; }
	 * 
	 * public void setBtnActiveSavedTemplatesList(List <WebElement>
	 * BtnActiveSavedTemplatesList) { this.BtnActiveSavedTemplatesList =
	 * BtnActiveSavedTemplatesList; }
	 */

	@FindBy(how = How.XPATH, using = "//*[@id='library']/div[1]/div/div/div[2]/h3") 
	public List <WebElement> LibraryTemplatesTitleList;

	public List <WebElement> getLibraryTemplatesTitleList() {
		return LibraryTemplatesTitleList;
	}

	@FindBy(how = How.XPATH, using = "//*[@class='tabbable tabbable-custom tabbable-full-width']/ul/li/a") 
	public List <WebElement> BtnDesignTopTabList;

	public List <WebElement> getBtnDesignTopTabList() {
		return BtnDesignTopTabList;
	}

	@FindBy(how = How.XPATH, using = "//*[@class='btn default']/i") 
	public List <WebElement> BtnEditTemplatesList;

	public List <WebElement> getBtnEditTemplatesList() {
		return BtnEditTemplatesList;
	}

	@FindBy(how = How.NAME, using = "create") 
	public WebElement BtnTemplateSave;

	public WebElement getBtnTemplateSave() {
		return BtnTemplateSave;
	}

	@FindBy(how = How.XPATH, using = "//*[@class='page-breadcrumb pull-left']/li") 
	public WebElement LblTemplateUrl;

	public WebElement getLblTemplateUrl() {
		return LblTemplateUrl;
	}

	@FindBy(how = How.ID, using = "contest_alias") 
	public WebElement LblTemplateContestUrl;

	public WebElement getLblTemplateContestUrl() {
		return LblTemplateContestUrl;
	}

	@FindBy(how = How.XPATH, using = "//*[@class='spash-text']/h1") 
	public WebElement ContentTitleNewTemplateTab;

	public WebElement getContentTitleNewTemplateTab() {
		return ContentTitleNewTemplateTab;
	}

	//Manage -> Settings -> Voting & Judging
	@FindBy(how = How.XPATH, using = "//*[@id='schedule_table_filter']/label/input") 
	public WebElement TxtSearchPeriod;

	public WebElement getTxtSearchPeriod() {
		return TxtSearchPeriod;
	}
	
	@FindBy(how = How.XPATH, using = "//*[@id='schedule_table']/tbody/tr/td[1]") 
	public WebElement GridRow1PeriodName;

	public WebElement getGridRow1PeriodName() {
		return GridRow1PeriodName;
	}
	
	@FindBy(how = How.XPATH, using = "//a[@class='btn green']") 
	public WebElement BtnAddPeriod;

	public WebElement getBtnAddPeriod() {
		return BtnAddPeriod;
	}

	@FindBy(how = How.XPATH, using = "//a[@class='btn green']/following-sibling::ul/li/a") 
	public List <WebElement> BtnAddPeriodTypeList;

	public List <WebElement> getBtnAddPeriodTypeList() {
		return BtnAddPeriodTypeList;
	}
	
	//Create Voting Period screen
		@FindBy(how = How.ID, using = "input_schedule_name") 
		public WebElement TxtVotingPeriodName;
		
		public WebElement getTxtVotingPeriodName() {
			return TxtVotingPeriodName;
		}

		public void setTxtVotingPeriodName(WebElement TxtVotingPeriodName) {
			this.TxtVotingPeriodName = TxtVotingPeriodName;
		}
		
		@FindBy(how = How.ID, using = "input_vote_start") 
		public WebElement DtVoteStart;
		
		public WebElement getDtVoteStart() {
			return DtVoteStart;
		}

		public void setDtVoteStart(WebElement DtVoteStart) {
			this.DtVoteStart = DtVoteStart;
		}
		
		@FindBy(how = How.ID, using = "input_vote_end") 
		public WebElement DtVoteEnd;
		
		public WebElement getDtVoteEnd() {
			return DtVoteEnd;
		}

		public void setDtVoteEnd(WebElement DtVoteEnd) {
			this.DtVoteEnd = DtVoteEnd;
		}
		
		@FindBy(how = How.ID, using = "enable_vote_template_form") 
		public WebElement DdVotingForm;
		
		public WebElement getDdVotingForm() {
			return DdVotingForm;
		}
		
		@FindBy(how = How.ID, using = "input_vote_limit_per") 
		public WebElement DdVoteLimitPer;
		
		public WebElement getDdVoteLimitPer() {
			return DdVoteLimitPer;
		}
		
		@FindBy(how = How.ID, using = "input_entry_eligibility") 
		public WebElement DdEntryEligibility;
		
		public WebElement getDdEntryEligibility() {
			return DdEntryEligibility;
		}
		
		@FindBy(how = How.NAME, using = "save") 
		public WebElement BtnSubmitVotingPeriod;
	
		public WebElement getBtnSubmitVotingPeriod() {
			return BtnSubmitVotingPeriod;
		}

		//Create Judging Period Screen
		@FindBy(how = How.ID, using = "input_judge_round_name") 
		public WebElement TxtRoundName;
		
		public WebElement getTxtRoundName() {
			return TxtRoundName;
		}

		public void setTxtRoundName(WebElement TxtRoundName) {
			this.TxtRoundName = TxtRoundName;
		}
		
		@FindBy(how = How.ID, using = "round_instruction") 
		public WebElement TxtRoundInstructions;
		
		public WebElement getTxtRoundInstructions() {
			return TxtRoundInstructions;
		}

		public void setTxtRoundInstructions(WebElement TxtRoundInstructions) {
			this.TxtRoundInstructions = TxtRoundInstructions;
		}
		
		@FindBy(how = How.XPATH, using = "//*[@id='input_start']") 
		public WebElement DtJudgingStartDate;
		
		public WebElement getDtJudgingStartDate() {
			return DtJudgingStartDate;
		}
		
		@FindBy(how = How.XPATH, using = "//*[@id='input_end']") 
		public WebElement DtJudgingEndDate;
		
		public WebElement getDtJudgingEndDate() {
			return DtJudgingEndDate;
		}
		
		@FindBy(how = How.ID, using = "s2id_autogen1") 
		public WebElement DdJudgesAssigned;
		
		public WebElement getDdJudgesAssigned() {
			return DdJudgesAssigned;
		}
		
		@FindBy(how = How.XPATH, using = "//button[@type='submit']") 
		public WebElement BtnSubmitJudgingPeriod;
		
		public WebElement getBtnSubmitJudgingPeriod() {
			return BtnSubmitJudgingPeriod;
		}
		
		@FindBy(how = How.XPATH, using = "//*[@id='select2-drop']/ul/li/div") 
		public List <WebElement> DdJudgesAssignedOptionsList;

		public List <WebElement> getDdJudgesAssignedOptionsList() {
			return DdJudgesAssignedOptionsList;
		}

//Contest -> Manage -> Category
		@FindBy(how = How.XPATH, using = "//a[contains(text(),'Create a new Category')]") 
		public WebElement BtnCreateNewCategory;

		public WebElement getBtnCreateNewCategory() {
			return BtnCreateNewCategory;
		}
		
		@FindBy(how = How.ID, using = "input_contest_category_name") 
		public WebElement TxtCategoryTitle;
		
		public WebElement getTxtCategoryTitle() {
			return TxtCategoryTitle;
		}
		
		@FindBy(how = How.ID, using = "input_template_id") 
		WebElement DdCategoryEntryForm;

		public WebElement getDdCategoryEntryForm() {
			return DdCategoryEntryForm;
		}
		
		@FindBy(how = How.XPATH, using = "//a[contains(text(),'Create')]") 
		public WebElement BtnSaveCategory;
	
		public WebElement getBtnSaveCategory() {
			return BtnSaveCategory;
		}
		
		@FindBy(how = How.XPATH, using = "//*[@id='contest_overall']/tbody/tr/td/div/div") 
		public List <WebElement> LblCategoryNameList;
	
		public List <WebElement> getLblCategoryNameList() {
			return LblCategoryNameList;
		}
		
		
//Contest -> Manage -> Add Entry
		@FindBy(how = How.XPATH, using = "//*[@class='fileinput-button']/a[1]") 
		public WebElement BtnSelectFromFile;
	
		public WebElement getBtnSelectFromFile() {
			return BtnSelectFromFile;
		}
		
		@FindBy(how = How.XPATH, using = "//*[contains(text(),'Your upload was successful')]") 
		public WebElement LblUploadSuccess;
	
		public WebElement getLblUploadSuccess() {
			return LblUploadSuccess;
		}
		
		@FindBy(how = How.XPATH, using = "//*[@id='s2id_input_user_id']/a/span[1]") 
		public WebElement LblChooseUser;
	
		public WebElement getLblChooseUser() {
			return LblChooseUser;
		}
		
		@FindBy(how = How.XPATH, using = "//*[@id='s2id_autogen1_search']") 
		public WebElement TxtChooseUser;
	
		public WebElement getTxtChooseUser() {
			return TxtChooseUser;
		}
		
		@FindBy(how = How.XPATH, using = "//*[@id='select2-results-1']/li") 
		public List <WebElement> AutoSuggOptUserList;
	
		public List <WebElement> getAutoSuggOptUserList() {
			return AutoSuggOptUserList;
		}
		
		@FindBy(how = How.XPATH, using = "//*[@id='input_title']") 
		public WebElement TxtEnrtyTitle;
	
		public WebElement getTxtEnrtyTitle() {
			return TxtEnrtyTitle;
		}
		
		@FindBy(how = How.XPATH, using = "//*[@id='description']") 
		public WebElement TxtEnrtyDesc;
	
		public WebElement getTxtEnrtyDesc() {
			return TxtEnrtyDesc;
		}
		
		@FindBy(how = How.XPATH, using = "//*[@id='input_contest_entry_user_email']") 
		public WebElement TxtEnrtyEmail;
	
		public WebElement getTxtEnrtyEmail() {
			return TxtEnrtyEmail;
		}
		
		@FindBy(how = How.XPATH, using = "//*[@id='uniform-form_tos']/span") 
		public WebElement CbTermConditions;
	
		public WebElement getCbTermConditions() {
			return CbTermConditions;
		}
		
		@FindBy(how = How.XPATH, using = "//*[@name='submit']") 
		public WebElement BtnSaveEntry;
	
		public WebElement getBtnSaveEntry() {
			return BtnSaveEntry;
		}
		
		
		
		
		

}






