package LaunchpadObjects;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class Dashboard_Objects {

	public WebDriver driver;
	boolean verify = false;

	public Dashboard_Objects(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.XPATH, using = "//*[@class='top-menu']/ul/li[2]/p/a") 
	public WebElement BtnUpgrade;

	public WebElement getBtnUpgrade(){
		return BtnUpgrade;
	}
	
	@FindBy(how = How.XPATH, using = "//*[@class='page-logo']/a") 
	public WebElement BtnHome;

	public WebElement getBtnHome(){
		return BtnHome;
	}
	
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Settings')][@class='sr-only']/parent::a/i") 
	public WebElement BtnSettings;

	public WebElement getBtnSettings(){
		return BtnSettings;
	}
	
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Users')][@class='sr-only']/parent::a/i") 
	public WebElement BtnUser;

	public WebElement getBtnUser(){
		return BtnUser;
	}
	
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Support')][@class='sr-only']/parent::a/i") 
	public WebElement BtnSupport;

	public WebElement getBtnSupport(){
		return BtnSupport;
	}
	
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Users')][@class='sr-only']/parent::a/parent::li/ul/li/a") 
	public List <WebElement> BtnUserOptions;

	public List <WebElement> getBtnUserOptions(){
		return BtnUserOptions;
	}
	
	@FindBy(how = How.XPATH, using = "//h3[contains(text(),'Dashboard')]") 
	public WebElement TitleDashboard;

	public WebElement getTitleDashboard(){
		return TitleDashboard;
	}
	
	@FindBy(how = How.XPATH, using = "//*[@class='menu-toggler sidebar-toggler']") 
	public WebElement BtnMenuToggle;

	public WebElement getBtnMenuToggle(){
		return BtnMenuToggle;
	}
	
	@FindBy(how = How.XPATH, using = "//*[@class='classic-menu-dropdown']/a") 
	public WebElement BtnVisitSite;

	public WebElement getBtnVisitSite(){
		return BtnVisitSite;
	}
	
	@FindBy(how = How.XPATH, using = "//*[@id='sortable_portlets']/div/div/div/div[2]/div/div[2]/div[1]") 
	public List <WebElement> LblFrameCounts;

	public List <WebElement> getLblFrameCounts(){
		return LblFrameCounts;
	}
	
	@FindBy(how = How.XPATH, using = "//*[@id='sortable_portlets']/div/div/div/div[2]/div/div[2]/div[2]") 
	public List <WebElement> LblFrameTitle;

	public List <WebElement> getLblFrameTitle(){
		return LblFrameTitle;
	}
	
	@FindBy(how = How.XPATH, using = "//*[@class='slimScrollDiv']/ul/li/a/i") 
	public List <WebElement> MenuBar;

	public List <WebElement> getMenuBar(){
		return MenuBar;
	}
	
//Date
	@FindBy(how = How.XPATH, using = "//*[@id='dashboard-report-range']") 
	public WebElement BtnCalander;

	public WebElement getBtnCalander(){
		return BtnCalander;
	}
	
	@FindBy(how = How.XPATH, using = "//*[@id='dashboard-report-range']/span") 
	public WebElement LblSelectedDateRange;

	public WebElement getLblSelectedDateRange(){
		return LblSelectedDateRange;
	}
	
	@FindBy(how = How.XPATH, using = "/*[@name='daterangepicker_end']") 
	public WebElement TxtStartDate;

	public WebElement TxtStartDate(){
		return TxtStartDate;
	}
	
	@FindBy(how = How.XPATH, using = "/*[@name='daterangepicker_end']") 
	public WebElement TxtEndDate;

	public WebElement getTxtEndDate(){
		return TxtEndDate;
	}
	
	@FindBy(how = How.XPATH, using = "//*[@class='ranges']/ul/li") 
	public List <WebElement> BtnDateRange;

	public List <WebElement> getBtnDateRange(){
		return BtnDateRange;
	}
	
	public void selectMainMenuBar(String MenuOption) {
		if(MenuOption.equalsIgnoreCase("Dashboard")) { 
			MenuBar.get(0).click(); 
		}
		if(MenuOption.equalsIgnoreCase("Contests")) { 
			MenuBar.get(1).click(); 
		}
		if(MenuOption.equalsIgnoreCase("User & Groups")) { 
			MenuBar.get(2).click(); 
		}
	}
	
	public void isDashboardElementDisplayed(String ElementName) {
		if(ElementName.equalsIgnoreCase("Upgrade")) {
			verify = getBtnCalander().isDisplayed();
		}
		if(ElementName.equalsIgnoreCase("Home")) {
			verify = getBtnHome().isDisplayed();
		}
		if(ElementName.equalsIgnoreCase("Settings")) {
			verify = getBtnSettings().isDisplayed();
		}
		if(ElementName.equalsIgnoreCase("User")) {
			verify = getBtnUser().isDisplayed();
		}
		if(ElementName.equalsIgnoreCase("Support")) {
			verify = getBtnSupport().isDisplayed();
		}
		if(ElementName.equalsIgnoreCase("Dashboard")) {
			verify = getTitleDashboard().isDisplayed();
		}
		if(ElementName.equalsIgnoreCase("Menu")) {
			verify = getBtnMenuToggle().isDisplayed();
		}
		if(ElementName.equalsIgnoreCase("VisitSite")) {
			verify = getBtnVisitSite().isDisplayed();
		}
		if(ElementName.equalsIgnoreCase("NewUser")) {
			verify = getLblFrameCounts().get(0).isDisplayed();
		}
		if(ElementName.equalsIgnoreCase("Streams")) {
			verify = getLblFrameCounts().get(1).isDisplayed();
		}
		if(ElementName.equalsIgnoreCase("Comments")) {
			verify = getLblFrameCounts().get(2).isDisplayed();
		}
		if(ElementName.equalsIgnoreCase("Votes")) {
			verify = getLblFrameCounts().get(3).isDisplayed();
		}
		if(ElementName.equalsIgnoreCase("NewUserTitle")) {
			verify = getLblFrameTitle().get(0).isDisplayed();
		}
		if(ElementName.equalsIgnoreCase("StreamsTitle")) {
			verify = getLblFrameTitle().get(1).isDisplayed();
		}
		if(ElementName.equalsIgnoreCase("CommentsTitle")) {
			verify = getLblFrameTitle().get(2).isDisplayed();
		}
		if(ElementName.equalsIgnoreCase("VotesTitle")) {
			verify = getLblFrameTitle().get(3).isDisplayed();
		}
		if(ElementName.equalsIgnoreCase("DashboardMenu")) {
			verify = getMenuBar().get(0).isDisplayed();
		}
		if(ElementName.equalsIgnoreCase("ContestsMenu")) {
			verify = getMenuBar().get(1).isDisplayed();
		}
		if(ElementName.equalsIgnoreCase("UsersGroupsMenu")) {
			verify = getMenuBar().get(2).isDisplayed();
		}
		if(ElementName.equalsIgnoreCase("Calander")) {
			verify = getBtnCalander().isDisplayed();
		}
		if(ElementName.equalsIgnoreCase("SelectedDate")) {
			verify = getLblSelectedDateRange().isDisplayed();
		}
		Assert.assertEquals(verify, true);
	}
	
	


}
