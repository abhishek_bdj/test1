package Launchpad6_objects;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Contest_Object {

	public WebDriver driver;

	public Contest_Object(WebDriver driver) {

		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(css = "a[class='menu_item_link_admin_contest']")
	WebElement contestmenu;

	@FindBy(xpath = "//tbody//tr")
	List<WebElement> replylistbackend;

	@FindBy(css = "button[id='prv_btn']")
	WebElement preview;

	public WebElement getPreview() {
		return preview;
	}

	public void setPreview(WebElement preview) {
		this.preview = preview;
	}

	public List<WebElement> getReplylistbackend() {
		return replylistbackend;
	}

	public void setReplylistbackend(List<WebElement> replylistbackend) {
		this.replylistbackend = replylistbackend;
	}

	@FindBy(xpath = "//descendant::i[@class='fa fa-pencil'][2]")
	WebElement templatelibraryedu;

	@FindBy(xpath="//h3[contains(text(),'Single Page')]//ancestor::div[1]//a")
	WebElement SinglePageedit;
	
	public WebElement getSinglePageedit() {
		return SinglePageedit;
	}

	public void setSinglePageedit(WebElement singlePageedit) {
		SinglePageedit = singlePageedit;
	}

	@FindBy(css = "button[type='submit']")
	WebElement tempaltelibrsave;

	@FindBy(xpath = "//descendant::a[@class='btn default'][2]")
	WebElement confirmdesigncode;

	@FindBy(css = "ul[class='page-breadcrumb pull-left']")
	WebElement frontendpart1;

	@FindBy(css = "input[name='contest_alias']")
	WebElement frontendpart2;

	public WebElement getTemplatelibraryedu() {
		return templatelibraryedu;
	}

	public WebElement getFrontendpart1() {
		return frontendpart1;
	}

	public void setFrontendpart1(WebElement frontendpart1) {
		this.frontendpart1 = frontendpart1;
	}

	public WebElement getFrontendpart2() {
		return frontendpart2;
	}

	public void setFrontendpart2(WebElement frontendpart2) {
		this.frontendpart2 = frontendpart2;
	}

	public WebElement getConfirmdesigncode() {
		return confirmdesigncode;
	}

	public void setConfirmdesigncode(WebElement confirmdesigncode) {
		this.confirmdesigncode = confirmdesigncode;
	}

	public void setTemplatelibraryedu(WebElement templatelibraryedu) {
		this.templatelibraryedu = templatelibraryedu;
	}

	public WebElement getTempaltelibrsave() {
		return tempaltelibrsave;
	}

	public void setTempaltelibrsave(WebElement tempaltelibrsave) {
		this.tempaltelibrsave = tempaltelibrsave;
	}

	@FindBy(xpath = "//div[contains(text(),'No saved')]")
	WebElement nosavedtemplate;

	public WebElement getNosavedtemplate() {
		return nosavedtemplate;
	}

	public void setNosavedtemplate(WebElement nosavedtemplate) {
		this.nosavedtemplate = nosavedtemplate;
	}

	@FindBy(xpath = "//table[@id='datatable']//tbody//tr/td[1]")
	List<WebElement> entryformfields;

	@FindBy(css = "button[name='entry_submit']")
	WebElement Contestentrysumbit;

	@FindBy(css = "b[role='presentation']")
	WebElement recorddropdown;
	@FindBy(xpath = "//div[@id='select2-drop']//li//div")
	List<WebElement> recordlist;

	@FindBy(xpath = "//tr//td[1]")
	List<WebElement> idcount;

	@FindBy(css = "input[id='confirmDeleteText']")
	WebElement deleteconfirmcontest;

	@FindBy(css = "button[id='delete_contest']")
	WebElement deletecontestbutton;

	@FindBy(xpath = "//td[contains(text(),'No matching records')]")
	WebElement noMatchingRecords;

	@FindBy(css = "a[class='btn default']")
	WebElement columns;

	@FindBy(xpath = "//div[@class='column_toggler dropdown-menu hold-on-click dropdown-checkboxes pull-right']//label")
	List<WebElement> columnlist;

	@FindBy(xpath = "//tr[@role='row']//th")
	List<WebElement> rowlist;

	public List<WebElement> getRowlist() {
		return rowlist;
	}

	public void setRowlist(List<WebElement> rowlist) {
		this.rowlist = rowlist;
	}

	public WebElement getColumns() {
		return columns;
	}

	public void setColumns(WebElement columns) {
		this.columns = columns;
	}

	public List<WebElement> getColumnlist() {
		return columnlist;
	}

	public void setColumnlist(List<WebElement> columnlist) {
		this.columnlist = columnlist;
	}

	public WebElement getNoMatchingRecords() {
		return noMatchingRecords;
	}

	public void setNoMatchingRecords(WebElement noMatchingRecords) {
		this.noMatchingRecords = noMatchingRecords;
	}

	public WebElement getDeletecontestbutton() {
		return deletecontestbutton;
	}

	public void setDeletecontestbutton(WebElement deletecontestbutton) {
		this.deletecontestbutton = deletecontestbutton;
	}

	public WebElement getDeleteconfirmcontest() {
		return deleteconfirmcontest;
	}

	public void setDeleteconfirmcontest(WebElement deleteconfirmcontest) {
		this.deleteconfirmcontest = deleteconfirmcontest;
	}

	@FindBy(xpath = "//button[contains(text(),'Delete')]")
	WebElement deletecontest;

	public WebElement getDeletecontest() {
		return deletecontest;
	}

	public void setDeletecontest(WebElement deletecontest) {
		this.deletecontest = deletecontest;
	}

	public List<WebElement> getIdcount() {
		return idcount;
	}

	public void setIdcount(List<WebElement> idcount) {
		this.idcount = idcount;
	}

	public WebElement getRecorddropdown() {
		return recorddropdown;
	}

	public void setRecorddropdown(WebElement recorddropdown) {
		this.recorddropdown = recorddropdown;
	}

	public List<WebElement> getRecordlist() {
		return recordlist;
	}

	public void setRecordlist(List<WebElement> recordlist) {
		this.recordlist = recordlist;
	}

	public WebElement getContestentrysumbit() {
		return Contestentrysumbit;
	}

	public void setContestentrysumbit(WebElement contestentrysumbit) {
		Contestentrysumbit = contestentrysumbit;
	}

	public List<WebElement> getEntryformfields() {
		return entryformfields;
	}

	public void setEntryformfields(List<WebElement> entryformfields) {
		this.entryformfields = entryformfields;
	}

	@FindBy(css = "button[class='btn green create-contest-btn']")
	WebElement Createanewcontest;

	@FindBy(css = "a[id='add-field']")
	WebElement addfieldentryform;

	@FindBy(css = "select[id='input_type']")
	WebElement uploadtype;

	@FindBy(css = "label[for='description']")
	WebElement ButtonLabel;

	@FindBy(css = "a[class='btn btn-primary btn-default select']")
	WebElement Select;

	public WebElement getSelect() {
		return Select;
	}

	public void setSelect(WebElement select) {
		Select = select;
	}

	@FindBy(css = "label[for='show_as_gallery']")
	WebElement showasgallery;

	@FindBy(css = "i[class='fa fa-arrow-circle-left']")
	WebElement iframebackbutton;

	@FindBy(css = "span[class*='off bootstrap-switch-default']")
	WebElement featuretoggle;

	public WebElement getIframebackbutton() {
		return iframebackbutton;
	}

	public void setIframebackbutton(WebElement iframebackbutton) {
		this.iframebackbutton = iframebackbutton;
	}

	public WebElement getButtonLabel() {
		return ButtonLabel;
	}

	public void setButtonLabel(WebElement buttonLabel) {
		ButtonLabel = buttonLabel;
	}

	public WebElement getShowasgallery() {
		return showasgallery;
	}

	public void setShowasgallery(WebElement showasgallery) {
		this.showasgallery = showasgallery;
	}

	public WebElement getUploadtype() {
		return uploadtype;
	}

	public void setUploadtype(WebElement uploadtype) {
		this.uploadtype = uploadtype;
	}

	@FindBy(css = "a[class='btn blue']")
	WebElement Moderationcommentbackend;

	@FindBy(css = "a[class='approve-selected btn btn-success btn-block']")
	WebElement ApproveSelectedbackend;
	@FindBy(css = "a[class='disapprove-selected btn btn-danger btn-block']")
	WebElement DisApproveSelectedbackend;

	public WebElement getDisApproveSelectedbackend() {
		return DisApproveSelectedbackend;
	}

	public void setDisApproveSelectedbackend(WebElement disApproveSelectedbackend) {
		DisApproveSelectedbackend = disApproveSelectedbackend;
	}

	@FindBy(css = "input[name='label']")
	WebElement Contestentrylabel;
	@FindBy(css = "button[data-action='delete']")
	WebElement deletecontestfield;

	@FindBy(xpath = "//descendant::button[@data-action='delete'][3]")
	WebElement deletecontestfieldvote;

	public WebElement getDeletecontestfieldvote() {
		return deletecontestfieldvote;
	}

	public void setDeletecontestfieldvote(WebElement deletecontestfieldvote) {
		this.deletecontestfieldvote = deletecontestfieldvote;
	}

	@FindBy(css = "button[id='confirm-btn']")
	WebElement deleteconfirm;

	public WebElement getDeleteconfirm() {
		return deleteconfirm;
	}

	public void setDeleteconfirm(WebElement deleteconfirm) {
		this.deleteconfirm = deleteconfirm;
	}

	public WebElement getDeletecontestfield() {
		return deletecontestfield;
	}

	public void setDeletecontestfield(WebElement deletecontestfield) {
		this.deletecontestfield = deletecontestfield;
	}

	public WebElement getAddfieldentryform() {
		return addfieldentryform;
	}

	public void setAddfieldentryform(WebElement addfieldentryform) {
		this.addfieldentryform = addfieldentryform;
	}

	public WebElement getContestentrylabel() {
		return Contestentrylabel;
	}

	public void setContestentrylabel(WebElement contestentrylabel) {
		Contestentrylabel = contestentrylabel;
	}

	public WebElement getActivetoggle() {
		return activetoggle;
	}

	public void setActivetoggle(WebElement activetoggle) {
		this.activetoggle = activetoggle;
	}

	@FindBy(xpath = "//label[@for='active']//ancestor::div[1]//div/div[1]")
	WebElement activetoggle;

	@FindBy(xpath = "//descendant::span[@class='bootstrap-switch-handle-off bootstrap-switch-default'][2]")
	WebElement publicEntrypageheader;

	public WebElement getPublicEntrypageheader() {
		return publicEntrypageheader;
	}

	public void setPublicEntrypageheader(WebElement publicEntrypageheader) {
		this.publicEntrypageheader = publicEntrypageheader;
	}

	@FindBy(css = "input[id='input_name']")
	WebElement contestname;

	@FindBy(css = "textarea[id=description]")
	WebElement Contestdescription;

	@FindBy(css = "button[id='create_new_contest_btn']")
	WebElement create;

	@FindBy(css = "input[type='search']")
	WebElement searchcontest;

	@FindBy(xpath = "//td[@class=' title']")
	WebElement Contestdescription1;

	@FindBy(xpath = "//td[@class=' text-middle']//button[2]")
	WebElement managecontestunique;

	@FindBy(xpath = "//td[@class=' text-middle']//button[1]")
	WebElement designcontestunique;

	public WebElement getDesigncontestunique() {
		return designcontestunique;
	}

	public void setDesigncontestunique(WebElement designcontestunique) {
		this.designcontestunique = designcontestunique;
	}

	@FindBy(xpath = "//button[contains(text(),'Manage')]")
	WebElement manageentryformbutton;

	@FindBy(xpath = "//descendant::button[contains(text(),'Manage')][3]")
	WebElement manageformtitle;

	public WebElement getManageformtitle() {
		return manageformtitle;
	}

	public void setManageformtitle(WebElement manageformtitle) {
		this.manageformtitle = manageformtitle;
	}

	@FindBy(xpath = "//descendant::button[contains(text(),'Manage')][2]")
	WebElement managevoteformbutton;

	@FindBy(xpath = "//ul[@class='nav nav-tabs']//li//a")
	List<WebElement> Contestmanagelist;

	@FindBy(xpath = "//ul[@class='list-unstyled plugin-nav']//li//a")
	List<WebElement> generalSettings;

	@FindBy(css = "a[id='add-field']")
	WebElement addform;

	@FindBy(xpath = "//table[@id='datatable']//td[1]")
	List<WebElement> addformlistsvoteentry;

	@FindBy(css = "ul[class='dropdown-menu pull-right'] li a")
	List<WebElement> addformlists;

	@FindBy(css = "select[id='input_contest_status']")
	WebElement statusdropdown;

	@FindBy(css = "button[type='submit']")
	WebElement Contestsettingsave;

	public WebElement getFeaturetoggle() {
		return featuretoggle;
	}

	public void setFeaturetoggle(WebElement featuretoggle) {
		this.featuretoggle = featuretoggle;
	}

	@FindBy(xpath = "//span[contains(text(),'Published')]")
	WebElement conteststatuscheck;

	@FindBy(css = "button[id='design_btn']")
	WebElement uniquedesign;

	@FindBy(xpath = "//ul[@class='nav nav-tabs']//li//a")
	List<WebElement> Designmenus;

	@FindBy(css = "i[class='fa fa-pencil']")
	WebElement editpencil;

	@FindBy(xpath = "//a[contains(text(),' View Entry page')]")
	WebElement ViewEntryPage;

	@FindBy(xpath = "//a[contains(text(),'Home page')]")
	WebElement HomePage;

	public WebElement getModerationcommentbackend() {
		return Moderationcommentbackend;
	}

	public void setModerationcommentbackend(WebElement moderationcommentbackend) {
		Moderationcommentbackend = moderationcommentbackend;
	}

	public WebElement getApproveSelectedbackend() {
		return ApproveSelectedbackend;
	}

	public void setApproveSelectedbackend(WebElement approveSelectedbackend) {
		ApproveSelectedbackend = approveSelectedbackend;
	}

	@FindBy(css = "option[value='featured']")
	WebElement featureditem;

	public WebElement getFeatureditem() {
		return featureditem;
	}

	public void setFeatureditem(WebElement featureditem) {
		this.featureditem = featureditem;
	}

	@FindBy(css = "select[id='input_home_gallery_show_entry']")
	WebElement showEntrySelect;

	@FindBy(css = "div[class='contest_item col-lg-4 col-md-4 col-sm-6'] a")
	List<WebElement> frontendentries;

	public List<WebElement> getFrontendentries() {
		return frontendentries;
	}

	public void setFrontendentries(List<WebElement> frontendentries) {
		this.frontendentries = frontendentries;
	}

	@FindBy(css = "button[class='btn btn-primary btn-m btn-block disable-button']")
	WebElement savetemplate;

	public WebElement getSavetemplate() {
		return savetemplate;
	}

	public void setSavetemplate(WebElement savetemplate) {
		this.savetemplate = savetemplate;
	}

	public WebElement getHomePage() {
		return HomePage;
	}

	public void setHomePage(WebElement homePage) {
		HomePage = homePage;
	}

	public WebElement getShowEntrySelect() {
		return showEntrySelect;
	}

	public void setShowEntrySelect(WebElement showEntrySelect) {
		this.showEntrySelect = showEntrySelect;
	}

	public WebDriver getDriver() {
		return driver;
	}

	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}

	public WebElement getEditpencil() {
		return editpencil;
	}

	public void setEditpencil(WebElement editpencil) {
		this.editpencil = editpencil;
	}

	public WebElement getViewEntryPage() {
		return ViewEntryPage;
	}

	public void setViewEntryPage(WebElement viewEntryPage) {
		ViewEntryPage = viewEntryPage;
	}

	public WebElement getViewEntryPagesocialSharing() {
		return ViewEntryPagesocialSharing;
	}

	public void setViewEntryPagesocialSharing(WebElement viewEntryPagesocialSharing) {
		ViewEntryPagesocialSharing = viewEntryPagesocialSharing;
	}

	public WebElement getCountdowntimer() {
		return Countdowntimer;
	}

	public void setCountdowntimer(WebElement countdowntimer) {
		Countdowntimer = countdowntimer;
	}

	@FindBy(css = "iframe[id='site_preview']")
	WebElement iframetemplate;

	public WebElement getIframetemplate() {
		return iframetemplate;
	}

	public void setIframetemplate(WebElement iframetemplate) {
		this.iframetemplate = iframetemplate;
	}

	@FindBy(css = "div[class='col-sm-12 social']")
	WebElement ViewEntryPagesocialSharing;

	@FindBy(css = "div[class='countdown is-countdown']")
	WebElement Countdowntimer;

	public List<WebElement> getDesignmenus() {
		return Designmenus;
	}

	public void setDesignmenus(List<WebElement> designmenus) {
		Designmenus = designmenus;
	}

	public WebElement getUniquedesign() {
		return uniquedesign;
	}

	public void setUniquedesign(WebElement uniquedesign) {
		this.uniquedesign = uniquedesign;
	}

	public WebElement getConteststatuscheck() {
		return conteststatuscheck;
	}

	public void setConteststatuscheck(WebElement conteststatuscheck) {
		this.conteststatuscheck = conteststatuscheck;
	}

	public WebElement getContestsettingsave() {
		return Contestsettingsave;
	}

	public void setContestsettingsave(WebElement contestsettingsave) {
		Contestsettingsave = contestsettingsave;
	}

	public WebElement getStatusdropdown() {
		return statusdropdown;
	}

	public void setStatusdropdown(WebElement statusdropdown) {
		this.statusdropdown = statusdropdown;
	}

	public WebElement getManagevoteformbutton() {
		return managevoteformbutton;
	}

	public void setManagevoteformbutton(WebElement managevoteformbutton) {
		this.managevoteformbutton = managevoteformbutton;
	}

	public WebElement getManageentryformbutton() {
		return manageentryformbutton;
	}

	public void setManageentryformbutton(WebElement manageentryformbutton) {
		this.manageentryformbutton = manageentryformbutton;
	}

	public WebElement getAddform() {
		return addform;
	}

	public void setAddform(WebElement addform) {
		this.addform = addform;
	}

	public List<WebElement> getAddformlists() {
		return addformlists;
	}

	public List<WebElement> getAddformlistsvoteentry() {
		return addformlistsvoteentry;
	}

	public void setAddformlistsvoteentry(List<WebElement> addformlistsvoteentry) {
		this.addformlistsvoteentry = addformlistsvoteentry;
	}

	public void setAddformlists(List<WebElement> addformlists) {
		this.addformlists = addformlists;
	}

	public WebElement getManagecontestunique() {
		return managecontestunique;
	}

	public void setManagecontestunique(WebElement managecontestunique) {
		this.managecontestunique = managecontestunique;
	}

	public List<WebElement> getContestmanagelist() {
		return Contestmanagelist;
	}

	public void setContestmanagelist(List<WebElement> contestmanagelist) {
		Contestmanagelist = contestmanagelist;
	}

	public List<WebElement> getGeneralSettings() {
		return generalSettings;
	}

	public void setGeneralSettings(List<WebElement> generalSettings) {
		this.generalSettings = generalSettings;
	}

	public void setSearchcontest(WebElement searchcontest) {
		this.searchcontest = searchcontest;
	}

	public void setContestdescription1(WebElement contestdescription1) {
		Contestdescription1 = contestdescription1;
	}

	public WebElement getSearchcontest() {
		return searchcontest;
	}

	public void setsearchcontest(WebElement dropdowncontest) {
		this.searchcontest = dropdowncontest;
	}

	public WebElement getContestdescription1() {
		return Contestdescription1;
	}

	public WebElement getContestmenu() {
		return contestmenu;
	}

	public void setContestmenu(WebElement contestmenu) {
		this.contestmenu = contestmenu;
	}

	public WebElement getCreateanewcontest() {
		return Createanewcontest;
	}

	public void setCreateanewcontest(WebElement createanewcontest) {
		Createanewcontest = createanewcontest;
	}

	public WebElement getContestname() {
		return contestname;
	}

	public void setContestname(WebElement contestname) {
		this.contestname = contestname;
	}

	public WebElement getContestdescription() {
		return Contestdescription;
	}

	public void setContestdescription(WebElement contestdescription) {
		Contestdescription = contestdescription;
	}

	public WebElement getCreate() {
		return create;
	}

	public void setCreate(WebElement create) {
		this.create = create;
	}

}
