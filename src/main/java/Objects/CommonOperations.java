package Objects;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class CommonOperations {
	
	WebDriver driver;
	public boolean verify = false;
	public WebDriverWait wait30;
	
	public CommonOperations(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	//Toast Message
	@FindBy(how = How.XPATH, using = "//*[@id='toast-container']/div/div[2]") 
	WebElement LabelToastMessage;
	
	@FindBy(how = How.XPATH, using = "//*[@id='toast-container']/div/div[1]")  
	WebElement LabelToastMessageTitle;
	
	public void selectFromDropdownList(WebDriver driver, WebElement DropdownElement, String DropdownValue) {
		Select select = new Select(DropdownElement);
		select.selectByVisibleText(DropdownValue);
	}
	
	public void selectFromDynamicDropdownList(WebDriver driver, WebElement DdElement, List <WebElement> DdOptionsList, String DdValue) {
		DdElement.click();
		for(int i = 0; i < DdOptionsList.size(); i++) {
			if(isElementTextContains(driver, DdOptionsList.get(i), DdValue) == true) {
				DdOptionsList.get(i).click();
			}
		}
	}
	
	public boolean isElementTextContains(WebDriver driver, WebElement element, String value) {
		if(element.getText().contains(value)) {
			verify = true;
		}
		return verify;
	}
	
	public void scrollDownTillElement(WebDriver driver, WebElement Element) {
		/*
		 * JavascriptExecutor executor = (JavascriptExecutor) driver;
		 * executor.executeScript("arguments[0].scrollIntoView(true);", Element);
		 */
		Actions actions = new Actions(driver);
		actions.moveToElement(Element);
		actions.perform();
	}
	
	public void Verify(boolean verify, boolean TrueOrFalse) {
		if(TrueOrFalse == true) {
			Assert.assertTrue(verify);
		}
		else {
			Assert.assertFalse(verify);
		}
	}
	
	public void verifyToastMessage(String message) {
		wait30 = new WebDriverWait(driver, 30);
		wait30.until(ExpectedConditions.visibilityOf(LabelToastMessage));
		String toastmessage = LabelToastMessage.getText();
		System.out.println("this is " + toastmessage);
		if(toastmessage.contains(message)){
			verify = true;
		}
		Verify(verify, true);
	}
	
	public void verifyToastMessage2(String message) {
		explicitlyWaitForVisibilityOfElement(driver, LabelToastMessageTitle);
		String toastmessage = LabelToastMessageTitle.getText();
		System.out.println("this is " + toastmessage);
		if(toastmessage.contains(message)){
			verify = true;
		}
		Verify(verify, true);
	}
	
	
	
	public boolean isElementPresent(WebDriver driver, WebElement element) {
		try {
			verify = element.isDisplayed();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return verify;
	}
	
	public static String getAlphaNumericString(int n)
    {
        // chose a Character random from this String
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
//                                    + "0123456789"
                                    + "abcdefghijklmnopqrstuvxyz";
  
        // create StringBuffer size of AlphaNumericString
        StringBuilder sb = new StringBuilder(n);
  
        for (int i = 0; i < n; i++) {
  
            // generate a random number between
            // 0 to AlphaNumericString variable length
            int index
                = (int)(AlphaNumericString.length()
                        * Math.random());
  
            // add Character one by one in end of sb
            sb.append(AlphaNumericString
                          .charAt(index));
        }
  
        return sb.toString();
    }
	
	public String CurrentDate()	{
		 SimpleDateFormat dtFormat = new SimpleDateFormat("dd-MM-yyyy");  
		 Calendar dtCalender = Calendar.getInstance();
		 String dtReturn = dtFormat.format(dtCalender.getTime());
		 return dtReturn;
	}

	public void explicitlyWaitForVisibilityOfElement(WebDriver driver, WebElement element) {
		wait30 = new WebDriverWait(driver, 30);
		wait30.until(ExpectedConditions.visibilityOf(element));
		verify = isElementPresent(driver, element);
		Verify(verify, true);
	}	
	
	public String[] handleWindows(WebDriver driver)
	{
		  Set<String> set =driver.getWindowHandles();
		  Iterator<String> itr= set.iterator();
		  String[] win=new String[5];
		  int i=0;
		  while (itr.hasNext()) 
		  {
			win[i]=itr.next();
			i++;
		  }
		  return win;
	}
	
	public void implicitlyWait(int seconds){
		driver.manage().timeouts().implicitlyWait(seconds, TimeUnit.SECONDS);
	}
	
}
