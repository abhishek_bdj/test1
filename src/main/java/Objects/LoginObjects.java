package Objects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class LoginObjects {
	
	public WebDriver driver;
	
	public LoginObjects(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.ID, using = "input_login_email") 
	WebElement TxtUsername;
	
	@FindBy(how = How.ID, using = "input_login_password") 
	WebElement TxtPassword;
	
	@FindBy(how = How.NAME, using = "continue") 
	WebElement BtnLogin;
	
	
	public void enterUsername() {
		TxtUsername.sendKeys("sachin.nair+111018@testriq.com");
	}
	
	public void enterPassword() {
		TxtPassword.sendKeys("Sachin*141#");
	}
	
	public void clickBtnLogin() {
		BtnLogin.click();
	}
	
	public void login() throws InterruptedException {
		TxtUsername.sendKeys("sachin.nair+111018@testriq.com");
		TxtPassword.sendKeys("Sachin*141#");
		//driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
//		WebDriverWait wait = new WebDriverWait(driver, 20); 
//		wait.until(ExpectedConditions.elementToBeClickable(By.name("login")));
		Thread.sleep(5000);
		BtnLogin.click();
	}
	

}
