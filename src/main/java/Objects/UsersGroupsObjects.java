package Objects;

import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class UsersGroupsObjects {
	
	WebDriver driver;
	CommonOperations co;
	boolean verify = false; 
	public WebDriverWait wait;

	public UsersGroupsObjects(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
//User Page
	@FindBy(how = How.XPATH, using = "//ul[@class='nav nav-tabs']/li/a") 
	public List <WebElement> UserPageTopTabs;
	
	@FindBy(how = How.XPATH, using = "//h3[contains(text(),'User')]") 
	public WebElement UserPageTitle;
	
	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Add New User')]") 
	public WebElement BtnAddNewUser;
	
	@FindBy(how = How.XPATH, using = "//*[@id='usermanager_filter']/label/input") 
	public WebElement TxtSearchUser;
	
	@FindBy(how = How.XPATH, using = "//button[contains(text(),'Delete')][@class='user_delete btn btn-danger btn-sm']") 
	public WebElement BtnDeleteUser;
	
	@FindBy(how = How.ID, using = "delete_user") 
	public WebElement BtnConfirmDeleteUser;
	
	
//User page -> Add new User Fields
	@FindBy(how = How.ID, using = "input_user_email") 
	public WebElement TxtUserEmail;
	
	@FindBy(how = How.ID, using = "input_register_password") 
	public WebElement TxtUserPass;
	
	@FindBy(how = How.ID, using = "input_register_repassword") 
	public WebElement TxtUserConfirmPass;
	
	@FindBy(how = How.ID, using = "input_user_first_name") 
	public WebElement TxtUserFirstName;
	
	@FindBy(how = How.ID, using = "input_user_last_name") 
	public WebElement TxtUserLastname;
	
	@FindBy(how = How.ID, using = "input_0") 
	public WebElement BtnCreateUser_Save;
	
	@FindBy(how = How.NAME, using = "save") 
	public WebElement BtnSaveChanges;
	
	
//Groups
	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Add New Group')]") 
	public WebElement BtnAddNewGroup;
	
	@FindBy(how = How.XPATH, using = "//button[contains(text(),'Delete')][@class='group_delete btn btn-danger btn-sm']") 
	public WebElement BtnDeleteGroup;
	
	@FindBy(how = How.XPATH, using = "//*[@id='delete_group']") 
	public WebElement BtnConfirmDeleteGroup;
	
	@FindBy(how = How.XPATH, using = "//*[@id='groupmanager_filter']/label/input") 
	public WebElement TxtSearchGroup;
	
	@FindBy(how = How.ID, using = "input_group_name") 
	public WebElement TxtGroupName;
	
	@FindBy(how = How.XPATH, using = "//button[contains(text(),'Create group')]") 
	public WebElement BtnCreateGroup_Save;
	
	@FindBy(how = How.XPATH, using = "//*[@class='btn red']") 
	public WebElement BtnTools;
	
	@FindBy(how = How.XPATH, using = "//*[@id='send-reset-password']/parent::li/parent::ul/li/a") 
	public List <WebElement> BtnToolsOptionList;
	
	@FindBy(how = How.XPATH, using = "//*[@id='usermanager_filter']/label/input") 
	public WebElement TxtAddUsersInGroupSearchBar;
	
	@FindBy(how = How.XPATH, using = "//*[@id='usermanager']/tbody/tr/td/div/span/input") 
	public List <WebElement> CbSelectUserRow;
	
	@FindBy(how = How.XPATH, using = "//*[@class='user_accept btn blue']") 
	public WebElement BtnAccept;
	
	
	
	
//Users Methods =================================================
	public void verifyUserPageVisible(String pageName) {
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		if(pageName.equalsIgnoreCase("User")) {
			verify = UserPageTitle.isDisplayed();
		}
		if(pageName.equalsIgnoreCase("CreateNewUser")) {
			verify = TxtUserEmail.isDisplayed();
		}
		co = new CommonOperations(driver);
		co.Verify(verify, true);
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}
	
	public void clickAddNewUser() {
		BtnAddNewUser.click();
	}
	
	public void enterUserDetails(String Email, String Pass, String FName, String LName) throws InterruptedException {
		TxtUserEmail.sendKeys(Email);
		TxtUserPass.sendKeys(Pass);
		TxtUserConfirmPass.sendKeys(Pass);
		TxtUserFirstName.sendKeys(FName);
		TxtUserLastname.sendKeys(LName);
		BtnCreateUser_Save.click();
		Thread.sleep(5000);
		BtnSaveChanges.click();
	}
	
	public void clickUserTopTabName(String TabName) {
		for (int i = 0; i < UserPageTopTabs.size(); i++) {
			if(TabName.equalsIgnoreCase(UserPageTopTabs.get(i).getText())) {
				UserPageTopTabs.get(i).click();
			}
		}
	}
	
	public void searchUser(String Value) throws InterruptedException {
		TxtSearchUser.sendKeys(Value);
		Thread.sleep(5000);
	}
	
	public void clickDeleteUser() throws InterruptedException {
		BtnDeleteUser.click();
		Thread.sleep(5000);
		BtnConfirmDeleteUser.click();
	}
	
	
//Groups Methods ==================================================	
	public void addNewGroup(String GruoupName) throws Exception {
		BtnAddNewGroup.click();
		Thread.sleep(5000);
		TxtGroupName.sendKeys(GruoupName);
		BtnCreateGroup_Save.click();
	}
	
	public void clickOnToolsOptions(String OptionName) {
		for (int i = 0; i < BtnToolsOptionList.size(); i++) {
			if(OptionName.equalsIgnoreCase(BtnToolsOptionList.get(i).getText())) {
				BtnToolsOptionList.get(i).click();
			}
		}
	}
	
	public void addUsersInGroup(String Username) throws Exception {
		BtnTools.click();
		clickOnToolsOptions("Add Users");
		wait = new WebDriverWait(driver, 30);
		verify = co.isElementPresent(driver, TxtAddUsersInGroupSearchBar);
		co.Verify(verify, true);
		TxtAddUsersInGroupSearchBar.sendKeys(Username);
		Thread.sleep(5000);
		CbSelectUserRow.get(0).click();
		BtnAccept.click();
		co.verifyToastMessage("Successfully");
	}
	
	public void searchGroup(String Value) throws InterruptedException {
		TxtSearchGroup.sendKeys(Value);
		Thread.sleep(5000);
	}
	
	public void clickDeleteGroup() {
		BtnDeleteGroup.click();
	}
	
	public void clickConfirmDeleteGroup() {
		BtnConfirmDeleteGroup.click();
	}
	
	
}
