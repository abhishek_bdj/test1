package Objects;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class DashboardObjects {
	
	public WebDriver driver;
	
	public DashboardObjects(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.XPATH, using = "//*[@class='slimScrollDiv']/ul/li/a/i") 
	public List <WebElement> MenuBar;
	
	//public List <WebElement> MenuBar = driver.findElements(By.xpath("//*[@class='slimScrollDiv']/ul/li/a/i"));
	
	public List <WebElement> getMenuBar(){
		return MenuBar;
	}
	
	
	  public void selectMainMenuBar(String MenuOption) {
	  if(MenuOption.equalsIgnoreCase("Dashboard")) { MenuBar.get(0).click(); }
	  if(MenuOption.equalsIgnoreCase("Contests")) { MenuBar.get(1).click(); }
	  if(MenuOption.equalsIgnoreCase("User & Groups")) { MenuBar.get(2).click(); }
	  }
	 

}
