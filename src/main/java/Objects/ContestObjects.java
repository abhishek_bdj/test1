package Objects;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter.DEFAULT;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class ContestObjects {
	
	public WebDriver driver;
	boolean verify = false;
	public WebElement elememt = null;
	public CommonOperations co;
	public int SavedTemplateCount = 0;
	
	public ContestObjects(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.XPATH, using = "//*[contains(text(),'Contest')][@class='page-title']") 
	WebElement TitleContest;
	
	@FindBy(how = How.XPATH, using = "//*[@id='table_contestmanager']/div/div/div[2]/button") 
	WebElement BtnCreateNewContest;
	
	@FindBy(how = How.XPATH, using = "//*[contains(text(),'Create a new contest')]") 
	WebElement WindowCreateNewContest;
	
	@FindBy(how = How.XPATH, using = "//label[contains(text(),'Search')]/input") 
	WebElement TxtSearchContest;
	
	
	//WebElement TitleContest = driver.findElement(By.xpath("//*[contains(text(),'Contest')][@class='page-title']"));
	//WebElement BtnCreateNewContest = driver.findElement(By.xpath("//*[@id='table_contestmanager']/div/div/div[2]/button/i"));
	//WebElement WindowCreateNewContest = driver.findElement(By.xpath("//*[contains(text(),'Create a new contest')]"));
	
//Create a new contest popup
	@FindBy(how = How.ID, using = "input_name") 
	WebElement TxtContestname;
	
	@FindBy(how = How.ID, using = "description") 
	WebElement TxtContestDescription;
	
	@FindBy(how = How.ID, using = "create_new_contest_btn") 
	WebElement BtnSaveContest;
	
//Gird
	@FindBy(how = How.XPATH, using = "//*[@id='contestmanager']/tbody/tr[1]/td") 
	List <WebElement> GridRow1;
	
	@FindBy(how = How.XPATH, using = "//*[@id='contestmanager']/tbody/tr[1]/td[8]/button") 
	List <WebElement> ActionButtons;
	
//Manage Contests
	@FindBy(how = How.XPATH, using = "//*[@class='tabbable tabbable-custom tabbable-full-width']/ul/li/a") 
	public List <WebElement> ManageContestTabNameList;
	
//Contest Manage -> Settings -> General
	@FindBy(how = How.ID, using = "input_contest_status") 
	public WebElement ContestStatus;
	
	@FindBy(how = How.NAME, using = "save") 
	public WebElement BtnSettingGeneralSave;
	
//Contest Manage -> Settings
	@FindBy(how = How.XPATH, using = "//*[@class='list-unstyled plugin-nav']/li/a") 
	public List <WebElement> ManageContestLeftMenuNameList;
	
	//Contest Manage -> Settings -> Forms
	@FindBy(how = How.ID, using = "add-field") 
	public WebElement BtnAddForm;
	
	@FindBy(how = How.XPATH, using = "//*[@id='add-field']/following-sibling::ul/li/a") 
	List <WebElement> BtnAddForm_Type;
	
//Add Vote Entry Form Fields
	@FindBy(how = How.ID, using = "add-field") 
	public WebElement BtnAddFormField;
	
	@FindBy(how = How.ID, using = "input_label") 
	public WebElement TxtVoteFormLabel;
	
	@FindBy(how = How.ID, using = "input_placeholder") 
	public WebElement TxtVoteFormPlaceholder;
	
	@FindBy(how = How.ID, using = "input_min_length") 
	public WebElement TxtVoteFormMinLengthOfText;
	
	@FindBy(how = How.ID, using = "input_max_length") 
	public WebElement TxtVoteFormMaxLengthOfText;
	
	@FindBy(how = How.NAME, using = "submit") 
	public WebElement BtnFormDetailsSave;
	
//Delete Contests
	@FindBy(how = How.XPATH, using = "//*[contains(text(),'Are  you sure?')]") 
	public WebElement DeleteContestConfirmPopup;
	
	@FindBy(how = How.ID, using = "confirmDeleteText") 
	public WebElement TxtDeleteContest;
	
	@FindBy(how = How.ID, using = "delete_contest") 
	public WebElement BtnConfirmDeleteContest;
	
//Design Contests
	@FindBy(how = How.XPATH, using = "//*[@class='thumbnail']") 
	public List <WebElement> SavedTemplatesList;
	
	@FindBy(how = How.XPATH, using = "//*[@id='overview']/div[1]/div/div//h3") 
	public List <WebElement> SavedTemplatesTitleList;
	
	@FindBy(how = How.XPATH, using = "//*[@class='checker']/span/input") 
	public List <WebElement> CbDefaultSavedTemplatesList;
	
	@FindBy(how = How.XPATH, using = "//*[@class='btn default delete-btn']/i") 
	public List <WebElement> BtnDeleteSavedTemplatesList;
	
	@FindBy(how = How.XPATH, using = "//*[@id='delete_template']") 
	public WebElement BtnConfirmDeleteTemplates;
	
	@FindBy(how = How.XPATH, using = "//*[@class='bootstrap-switch-label']") 
	public List <WebElement> BtnActiveSavedTemplatesList;
	
	@FindBy(how = How.XPATH, using = "//*[@id='library']/div[1]/div/div/div[2]/h3") 
	public List <WebElement> LibraryTemplatesTitleList;
	
	@FindBy(how = How.XPATH, using = "//*[@class='tabbable tabbable-custom tabbable-full-width']/ul/li/a") 
	public List <WebElement> BtnDesignTopTabList;
	
	@FindBy(how = How.XPATH, using = "//*[@class='btn default']/i") 
	public List <WebElement> BtnEditTemplatesList;
	
	@FindBy(how = How.NAME, using = "create") 
	public WebElement BtnTemplateSave;
	
	@FindBy(how = How.XPATH, using = "//*[@class='page-breadcrumb pull-left']/li") 
	public WebElement LblTemplateUrl;
	
	@FindBy(how = How.ID, using = "contest_alias") 
	public WebElement LblTemplateContestUrl;
	
	@FindBy(how = How.XPATH, using = "//*[@class='spash-text']/h1") 
	public WebElement ContentTitleNewTemplateTab;
	
	
	
//Manage -> Settings -> Voting & Judging
	@FindBy(how = How.XPATH, using = "//a[@class='btn green']") 
	public WebElement BtnAddPeriod;
	
	@FindBy(how = How.XPATH, using = "//a[@class='btn green']/following-sibling::ul/li/a") 
	public List <WebElement> BtnAddPeriodTypeList;
	
//Create Voting Period screen
	@FindBy(how = How.ID, using = "input_schedule_name") 
	public WebElement TxtVotingPeriodName;
	
	@FindBy(how = How.ID, using = "input_vote_start") 
	public WebElement DtVoteStart;
	
	@FindBy(how = How.ID, using = "input_vote_end") 
	public WebElement DtVoteEnd;
	
	@FindBy(how = How.ID, using = "enable_vote_template_form") 
	public WebElement DdVotingForm;
	
	@FindBy(how = How.ID, using = "input_vote_limit_per") 
	public WebElement DdVoteLimitPer;
	
	@FindBy(how = How.ID, using = "input_entry_eligibility") 
	public WebElement DdEntryEligibility;
	
	@FindBy(how = How.NAME, using = "save") 
	public WebElement BtnSubmitVotingPeriod;
	
//Create Judging Period Screen
	@FindBy(how = How.ID, using = "input_judge_round_name") 
	public WebElement TxtRoundName;
	
	@FindBy(how = How.ID, using = "round_instruction") 
	public WebElement TxtRoundInstructions;
	
	@FindBy(how = How.XPATH, using = "//*[@id='input_start']") 
	public WebElement DtJudgingStartDate;
	
	@FindBy(how = How.XPATH, using = "//*[@id='input_end']") 
	public WebElement DtJudgingEndDate;
	
	@FindBy(how = How.ID, using = "s2id_autogen1") 
	public WebElement DdJudgesAssigned;
	
	@FindBy(how = How.XPATH, using = "//button[@type='submit']") 
	public WebElement BtnSubmitJudgingPeriod;
	
	@FindBy(how = How.XPATH, using = "//*[@id='select2-drop']/ul/li/div") 
	public List <WebElement> DdJudgesAssignedOptionsList;
	
	
//Methods
	public void verifyContestPageVisible(String pageName) {
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		if(pageName.equalsIgnoreCase("ContestPage")) {
			verify = TitleContest.isDisplayed();
		}
		if(pageName.equalsIgnoreCase("CreateContestPopup")) {
			verify = WindowCreateNewContest.isDisplayed();
		}
		Assert.assertTrue(verify);
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}
	
	public void clickCreateContestBtn() {
		BtnCreateNewContest.click();
	}
	
	public void enterContestDetails(String Contest, String Description) {
		TxtContestname.sendKeys(Contest);
		TxtContestDescription.sendKeys(Description);
	}
	
	public void clickSaveContestBtn() {
		BtnSaveContest.click();
	}
	
	public void VerifyContestSaved(String ContestName) {
		if(ContestName.equalsIgnoreCase(GridRow1.get(1).getText())) {
			verify = true;
			Assert.assertTrue(verify);
		}
	}
	
	public void clickonAction(String ButtonName) throws InterruptedException {
		Thread.sleep(5000);
		switch(ButtonName) {
		case "Design":
			elememt = ActionButtons.get(0);
			break;
			
		case "Manage":
			elememt = ActionButtons.get(1);
			break;
			
		case "Copy":
			elememt = ActionButtons.get(2);
			break;
			
		case "Delete":
			elememt = ActionButtons.get(3);
			break;
		}
		elememt.click();
	}
	
	public void clickManageContestTabName(String TabName) {
		for (int i = 0; i < ManageContestTabNameList.size(); i++) {
			if(TabName.equalsIgnoreCase(ManageContestTabNameList.get(i).getText())) {
				ManageContestTabNameList.get(i).click();
			}
		}
	}
	
	public void clickOnBtnSettingGeneralSave() {
		BtnSettingGeneralSave.click();
	}
	
	public void clickManageContestLeftMenuName(String MenuName) {
		for (int i = 0; i < ManageContestLeftMenuNameList.size(); i++) {
			if(MenuName.equalsIgnoreCase(ManageContestLeftMenuNameList.get(i).getText())) {
				ManageContestLeftMenuNameList.get(i).click();
			}
		}
	}
	
	public void clickAddForm() {
		BtnAddForm.click();
	}
	
	public void clickAddFormType(String ButtonName) {
		switch(ButtonName) {
		case "Entry": 
			elememt = BtnAddForm_Type.get(0);
			break;
			
		case "Vote": 
			elememt = BtnAddForm_Type.get(1);
			break;
			
		default:
			System.out.println("No Button Displayed");
			break;
		}
		elememt.click();
	}
	
	public void clickAddFormFields() {
		BtnAddFormField.click();
		co = new CommonOperations(driver);
		verify = co.isElementPresent(driver, TxtVoteFormLabel);
		co.Verify(verify, true);
	}
	
	public void enterVoteFormDetails(String Label, String Placeholder, String MinLength, String MaxLength) {
		/*
		 * String details[] = {Label, Placeholder, MinLength, MaxLength}; List <String>
		 * detailsList = Arrays.asList(details);
		 */
		TxtVoteFormLabel.click();
		TxtVoteFormLabel.sendKeys(Label);
		TxtVoteFormPlaceholder.sendKeys(Placeholder);
		TxtVoteFormPlaceholder.click();
		TxtVoteFormMinLengthOfText.sendKeys(MinLength);
		TxtVoteFormMinLengthOfText.click();
		TxtVoteFormMaxLengthOfText.sendKeys(MaxLength);
		TxtVoteFormMaxLengthOfText.click();
	}
	
	public void clickSaveFormFields() {
		BtnFormDetailsSave.click();
	}
	
	public void enterConfirmDeleteText() {
		TxtDeleteContest.sendKeys("DELETE");
	}
	
	public void clickOnConfirmDeleteContestBtn() {
		BtnConfirmDeleteContest.click();
	}
	
	public void searchContest(String Value) throws Exception {
		TxtSearchContest.sendKeys(Value);
		Thread.sleep(5000);
	}
	
	//Design Methods
	
	public void clickDesignScreenTab(String TabName) throws InterruptedException {
		for (int i = 0; i < BtnDesignTopTabList.size(); i++) {
			if(TabName.equalsIgnoreCase(BtnDesignTopTabList.get(i).getText())) {
				BtnDesignTopTabList.get(i).click();
			}
		}
		Thread.sleep(5000);
	}
	
	public void clickOnEditTemplate(int index) {
		BtnEditTemplatesList.get(index).click();
	}
	
	public void clickOnTemplateSaveBtn() {
		BtnTemplateSave.click();
	}
	
	public boolean verifySavedTemplatesCount(int PreCount) {
		if(PreCount < SavedTemplatesList.size()) {
			verify = true;
		}
		return verify;
	} 
	
	public void clickOnAddPeriod() {
		BtnAddPeriod.click();
	}
	
	public void clickAddPeriodType(String ButtonName) {
		switch(ButtonName) {
		case "Voting Period": 
			elememt = BtnAddPeriodTypeList.get(0);
			break;
			
		case "Judging Period": 
			elememt = BtnAddPeriodTypeList.get(1);
			break;
			
		default:
			System.out.println("No Button Displayed");
			break;
		}
		elememt.click();
	}
	
	public void addVotingPeriodDetails(String PeriodName, String StartDate, String EndDate, String VoteLimitPer, String EntryEligibility) {
		TxtVotingPeriodName.sendKeys(PeriodName);
		DtVoteStart.sendKeys(StartDate);
		DtVoteEnd.sendKeys(EndDate);
		//co.selectFromDropdownList(driver, DdVotingForm, VoteForm);
		co = new CommonOperations(driver);
		co.selectFromDropdownList(driver, DdVoteLimitPer, VoteLimitPer);
		co.selectFromDropdownList(driver, DdEntryEligibility, EntryEligibility);
		BtnSubmitVotingPeriod.click();
	}
	
	public void addJudgingPeriodDetails(String RoundName, String RoundInstructions, String StartDate, String EndDate, String JudgesAssigned) {
		TxtRoundName.sendKeys(RoundName);
		TxtRoundInstructions.sendKeys(RoundInstructions);
		DtJudgingStartDate.clear();
		DtJudgingStartDate.sendKeys(StartDate);
		DtJudgingEndDate.clear();
		DtJudgingEndDate.sendKeys(EndDate);
		co = new CommonOperations(driver);
		co.selectFromDynamicDropdownList(driver, DdJudgesAssigned, DdJudgesAssignedOptionsList, JudgesAssigned);
		//co.selectFromDropdownList(driver, DdJudgesAssigned, JudgesAssigned);
		BtnSubmitJudgingPeriod.click();
	}
	
	public void selectParticularTemplate(String TemplateTitle) throws InterruptedException {
		for(int i = 0; i < LibraryTemplatesTitleList.size(); i++) {
			if(LibraryTemplatesTitleList.get(i).getText().contains(TemplateTitle)){
				BtnEditTemplatesList.get(i).click();
			}
			Thread.sleep(5000);
		}
	}
	
	public void setDefaultSavedTemplate(String TemplateTitle) throws InterruptedException {
		for(int i = 0; i < SavedTemplatesTitleList.size(); i++) {
			if(SavedTemplatesTitleList.get(i).getText().contains(TemplateTitle)){
				BtnActiveSavedTemplatesList.get(i).click();
				Thread.sleep(5000);
				CbDefaultSavedTemplatesList.get(i).click();
			}
		}
	}
	
	public void resetDefaultSavedTemplate(String TemplateTitle) throws InterruptedException {
		for(int i = 0; i < SavedTemplatesTitleList.size(); i++) {
			if(SavedTemplatesTitleList.get(i).getText().contains(TemplateTitle)){
				CbDefaultSavedTemplatesList.get(i).click();
			}
		}
	}
	
	public String concatUrl() {
		String url = LblTemplateUrl.getText();
		String cont_url = LblTemplateContestUrl.getAttribute("value");
		System.out.println("this is cont url: " +  cont_url);
		return url+cont_url;
	}
	
	public void verifyNewTabContentTitle(String Value) {
		System.out.println(ContentTitleNewTemplateTab.getText());
		if(Value.equalsIgnoreCase(ContentTitleNewTemplateTab.getText())) {
			verify = true;
		}
		co = new CommonOperations(driver);
		co.Verify(verify, true);
	}
	
	public void deleteSavedTemplate(String TemplateTitle) {
		for(int i = 0; i < SavedTemplatesTitleList.size(); i++) {
			if(SavedTemplatesTitleList.get(i).getText().contains(TemplateTitle)){
				BtnDeleteSavedTemplatesList.get(i).click();
				co.explicitlyWaitForVisibilityOfElement(driver, BtnConfirmDeleteTemplates);
				BtnConfirmDeleteTemplates.click();
			}
		}
	}
	
}
