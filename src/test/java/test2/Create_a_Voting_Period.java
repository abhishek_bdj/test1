package test2;

import java.io.FileInputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import LaunchpadObjects.Contest_Objects;
import LaunchpadObjects.Dashboard_Objects;
import LaunchpadObjects.Login_Objects;
import Objects.BaseClass;
import Objects.CommonOperations;

public class Create_a_Voting_Period extends BaseClass{

	public WebDriver driver;
	Login_Objects login;
	Dashboard_Objects dashboard;
	Contest_Objects contest;
	CommonOperations co;
	Properties prop;
	boolean verify = false;
	int PreCount = 0;
	String backend, Frontend = null;

	@BeforeTest
	public void initialise() throws Exception {
		driver = setup();
		login = new Login_Objects(driver);
		dashboard = new Dashboard_Objects(driver);
		contest = new Contest_Objects(driver);
		co = new CommonOperations(driver);
		prop = new Properties();
		FileInputStream fis = new FileInputStream("C:\\Users\\ABI\\Test_workspace\\TestriqDemo\\src\\main\\java\\Resources\\TestData.properties");	//To read from file
		prop.load(fis);										//to load the data form the file
		driver.get(prop.getProperty("Url"));
		login.getTxtUsername().sendKeys(prop.getProperty("UserName"));
		login.getTxtPassword().sendKeys(prop.getProperty("Password"));
		login.getBtnLogin().click();
		co.explicitlyWaitForVisibilityOfElement(driver, dashboard.getMenuBar().get(0));
		verify = co.isElementPresent(driver, dashboard.getMenuBar().get(0));
		co.Verify(verify, true);
	}

	@Test
	public void creating_Voting_Period() throws InterruptedException {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		dashboard.getMenuBar().get(1).click();
		contest.getTxtSearchContest().sendKeys(prop.getProperty("ContestName"));
		Thread.sleep(4000);
		contest.getActionButtons().get(1).click();
		for (int i = 0; i < contest.getManageContestTabNameList().size(); i++) {
			if(contest.getManageContestTabNameList().get(i).getText().contains("Settings")) {
				contest.getManageContestTabNameList().get(i).click();
			}
		}
		for (int i = 0; i < contest.getManageContestLeftMenuNameList().size(); i++) {
			if(contest.getManageContestLeftMenuNameList().get(i).getText().contains("Voting & Judging")) {
				contest.getManageContestLeftMenuNameList().get(i).click();
			}
		}
		contest.getBtnAddPeriod().click();
		contest.getBtnAddPeriodTypeList().get(0).click();
		contest.getTxtVotingPeriodName().sendKeys(prop.getProperty("VotingPeriodName"));
		contest.getDtVoteStart().sendKeys(prop.getProperty("VoteStartDate"));
		contest.getDtVoteEnd().sendKeys(prop.getProperty("VoteEndDate"));
		Select selectVoteLimitPer = new Select(contest.getDdVoteLimitPer());
		selectVoteLimitPer.selectByVisibleText(prop.getProperty("VotingLimitPer"));
		Select selectEntryEligibility = new Select(contest.getDdEntryEligibility());
		selectEntryEligibility.selectByVisibleText(prop.getProperty("VoteEntryEligibility"));
		contest.getBtnSubmitVotingPeriod().click();
		contest.getTxtSearchPeriod().sendKeys(prop.getProperty("VotingPeriodName"));
		Thread.sleep(4000);
		if(contest.getGridRow1PeriodName().getText().contains(prop.getProperty("VotingPeriodName"))) {
			Assert.assertTrue(true);
		}
	}
	
	@AfterTest()
	public void afterexecution() {
		driver.close();
		driver.quit();
	}
}
