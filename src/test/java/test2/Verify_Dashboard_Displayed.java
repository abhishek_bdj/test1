package test2;

import java.io.FileInputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import LaunchpadObjects.Dashboard_Objects;
import LaunchpadObjects.Login_Objects;
import Objects.BaseClass;

public class Verify_Dashboard_Displayed extends BaseClass{
	
	public WebDriver driver;
	Login_Objects login;
	Dashboard_Objects dashboard;
	Properties prop;
	boolean verify = false;
	int PreCount = 0;
	String backend, Frontend = null;
	
	@BeforeTest
	public void initialise() throws Exception {
		driver = setup();
		login = new Login_Objects(driver);
		dashboard = new Dashboard_Objects(driver);
		prop = new Properties();
		FileInputStream fis = new FileInputStream("C:\\Users\\ABI\\Test_workspace\\TestriqDemo\\src\\main\\java\\Resources\\TestData.properties");	//To read from file
		prop.load(fis);										//to load the data form the file
		driver.get(prop.getProperty("Url"));
		login.getTxtUsername().sendKeys(prop.getProperty("UserName"));
		login.getTxtPassword().sendKeys(prop.getProperty("Password"));
		login.getBtnLogin().click();
	}
	
	@Test
	public void verify_Dashboard_Screen() throws InterruptedException {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		dashboard.isDashboardElementDisplayed("Upgrade");
		dashboard.isDashboardElementDisplayed("Home");
		dashboard.isDashboardElementDisplayed("Settings");
		dashboard.isDashboardElementDisplayed("User");
		dashboard.isDashboardElementDisplayed("Support");
		dashboard.isDashboardElementDisplayed("Dashboard");
		dashboard.isDashboardElementDisplayed("Menu");
		dashboard.isDashboardElementDisplayed("VisitSite");
		dashboard.isDashboardElementDisplayed("NewUser");
		dashboard.isDashboardElementDisplayed("Streams");
		dashboard.isDashboardElementDisplayed("Comments");
		dashboard.isDashboardElementDisplayed("Votes");
		dashboard.isDashboardElementDisplayed("NewUserTitle");
		dashboard.isDashboardElementDisplayed("StreamsTitle");
		dashboard.isDashboardElementDisplayed("CommentsTitle");
		dashboard.isDashboardElementDisplayed("VotesTitle");
		dashboard.isDashboardElementDisplayed("DashboardMenu");
		dashboard.isDashboardElementDisplayed("ContestsMenu");
		dashboard.isDashboardElementDisplayed("UsersGroupsMenu");
		dashboard.isDashboardElementDisplayed("Calander");
		dashboard.isDashboardElementDisplayed("SelectedDate");
	}
	
	@AfterTest()
	public void afterexecution() {
		driver.close();
		driver.quit();
	}

}
