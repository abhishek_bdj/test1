package test2;

import org.testng.annotations.Test;
import org.testng.Assert;
import org.testng.AssertJUnit;
import java.io.FileInputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import LaunchpadObjects.Contest_Objects;
import LaunchpadObjects.Dashboard_Objects;
import LaunchpadObjects.Login_Objects;
import LaunchpadObjects.Users_And_Groups_Objects;
import Objects.BaseClass;
import Objects.CommonOperations;

public class Create_a_User extends BaseClass{
	
	public WebDriver driver;
	Login_Objects login;
	Dashboard_Objects dashboard;
	Contest_Objects contest;
	Users_And_Groups_Objects users_groups;
	CommonOperations co;
	Properties prop;
	boolean verify = false;
	int PreCount = 0;
	String backend, Frontend = null;
	
	@BeforeTest
	public void initialise() throws Exception {
		driver = setup();
		login = new Login_Objects(driver);
		dashboard = new Dashboard_Objects(driver);
		contest = new Contest_Objects(driver);
		users_groups = new Users_And_Groups_Objects(driver);
		co = new CommonOperations(driver);
		prop = new Properties();
		FileInputStream fis = new FileInputStream("C:\\Users\\ABI\\Test_workspace\\TestriqDemo\\src\\main\\java\\Resources\\TestData.properties");	//To read from file
		prop.load(fis);										//to load the data form the file
		driver.get(prop.getProperty("Url"));
		login.getTxtUsername().sendKeys(prop.getProperty("UserName"));
		login.getTxtPassword().sendKeys(prop.getProperty("Password"));
		login.getBtnLogin().click();
		co.explicitlyWaitForVisibilityOfElement(driver, dashboard.getMenuBar().get(0));
		verify = co.isElementPresent(driver, dashboard.getMenuBar().get(0));
		co.Verify(verify, true);
	}
	
	@Test
	public void creating_a_User() throws InterruptedException {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		dashboard.getMenuBar().get(2).click();
		users_groups.getBtnAddNewUser().click();
		users_groups.getTxtUserEmail().sendKeys(prop.getProperty("UserEmail"));
		users_groups.getTxtUserPass().sendKeys(prop.getProperty("UserPassword"));
		users_groups.getTxtUserConfirmPass().sendKeys(prop.getProperty("UserPassword"));
		users_groups.getTxtUserFirstName().sendKeys(prop.getProperty("UserFname"));
		users_groups.getTxtUserLastname().sendKeys(prop.getProperty("UserLname"));
		users_groups.getBtnCreateUser_Save().click();
		dashboard.getMenuBar().get(2).click();
		users_groups.getTxtSearchUser().sendKeys(prop.getProperty("UserEmail"));
		Thread.sleep(4000);
		if(users_groups.getTxtGridRow1Email().getText().contains(prop.getProperty("UserEmail"))){
			Assert.assertTrue(true);
		}
	}
	
	@AfterTest()
	public void afterexecution() {
		driver.close();
		driver.quit();
	}

}
