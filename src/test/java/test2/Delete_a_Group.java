package test2;

import java.io.FileInputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import LaunchpadObjects.Contest_Objects;
import LaunchpadObjects.Dashboard_Objects;
import LaunchpadObjects.Login_Objects;
import LaunchpadObjects.Users_And_Groups_Objects;
import Objects.BaseClass;
import Objects.CommonOperations;

public class Delete_a_Group extends BaseClass{

	public WebDriver driver;
	Login_Objects login;
	Dashboard_Objects dashboard;
	Contest_Objects contest;
	Users_And_Groups_Objects users_groups;
	CommonOperations co;
	Properties prop;
	boolean verify = false;
	int PreCount = 0;
	String backend, Frontend = null;

	@BeforeTest
	public void initialise() throws Exception {
		driver = setup();
		login = new Login_Objects(driver);
		dashboard = new Dashboard_Objects(driver);
		contest = new Contest_Objects(driver);
		users_groups = new Users_And_Groups_Objects(driver);
		co = new CommonOperations(driver);
		prop = new Properties();
		FileInputStream fis = new FileInputStream("C:\\Users\\ABI\\Test_workspace\\TestriqDemo\\src\\main\\java\\Resources\\TestData.properties");	//To read from file
		prop.load(fis);										//to load the data form the file
		driver.get(prop.getProperty("Url"));
		login.getTxtUsername().sendKeys(prop.getProperty("UserName"));
		login.getTxtPassword().sendKeys(prop.getProperty("Password"));
		login.getBtnLogin().click();
		co.explicitlyWaitForVisibilityOfElement(driver, dashboard.getMenuBar().get(0));
		verify = co.isElementPresent(driver, dashboard.getMenuBar().get(0));
		co.Verify(verify, true);
	}

	@Test
	public void deleting_a_Group() throws InterruptedException {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		dashboard.getMenuBar().get(2).click();
		users_groups.getUserPageTopTabs().get(1).click();
		users_groups.getTxtSearchGroup().sendKeys(prop.getProperty("GroupName"));
		Thread.sleep(4000);
		users_groups.getBtnDeleteGroup().click();
		users_groups.getBtnConfirmDeleteGroup().click();
		if(users_groups.getLblGridNoRecordsFound().isDisplayed()) {
			Assert.assertTrue(true);
		}
	}
	
	@AfterTest()
	public void afterexecution() {
		driver.close();
		driver.quit();
	}

}
