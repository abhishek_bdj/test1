package test2;

import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import Objects.BaseClass;

public class TestListener extends BaseClass implements ITestListener{
	
	@Override
	public void onTestStart(ITestResult result) {
		System.out.println("Started");
	}

	@Override
	public void onTestSuccess(ITestResult result) {
		System.out.println("Passed listeners");
		
	}

	@Override
	public void onTestFailure(ITestResult result) {
		String testcaseName = result.getMethod().getMethodName();
		try {
			driver = (WebDriver)result.getTestClass().getRealClass().getDeclaredField("driver").get(result.getInstance());
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		try {
			BaseClass.getScreenshotPath(testcaseName, driver);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void onTestSkipped(ITestResult result) {
		
	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		
	}

	@Override
	public void onTestFailedWithTimeout(ITestResult result) {
		
	}

	@Override
	public void onStart(ITestContext context) {
		
	}

	@Override
	public void onFinish(ITestContext context) {
		
	}
	
		

}
