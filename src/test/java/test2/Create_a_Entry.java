package test2;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import LaunchpadObjects.Contest_Objects;
import LaunchpadObjects.Dashboard_Objects;
import LaunchpadObjects.FrontendWindow_Objects;
import LaunchpadObjects.Login_Objects;
import Objects.BaseClass;
import Objects.CommonOperations;

public class Create_a_Entry extends BaseClass{

	public WebDriver driver;
	Login_Objects login;
	Dashboard_Objects dashboard;
	Contest_Objects contest;
	Create_a_Voting_Period voteperiod;
	FrontendWindow_Objects frontend;
	CommonOperations co;
	Properties prop;
	boolean verify = false;
	int PreCount = 0;
	String backend, Frontend = null;

	@BeforeTest
	public void initialise() throws Exception {
		driver = setup();
		login = new Login_Objects(driver);
		dashboard = new Dashboard_Objects(driver);
		contest = new Contest_Objects(driver);
		voteperiod = new Create_a_Voting_Period();
		frontend = new FrontendWindow_Objects(driver);
		co = new CommonOperations(driver);
		wait30 = new WebDriverWait(driver, 30);
		prop = new Properties();
		FileInputStream fis = new FileInputStream("C:\\Users\\ABI\\Test_workspace\\TestriqDemo\\src\\main\\java\\Resources\\TestData.properties");	//To read from file
		prop.load(fis);										//to load the data form the file
		driver.get(prop.getProperty("Url"));
		login.getTxtUsername().sendKeys(prop.getProperty("UserName"));
		login.getTxtPassword().sendKeys(prop.getProperty("Password"));
		login.getBtnLogin().click();
		co.explicitlyWaitForVisibilityOfElement(driver, dashboard.getMenuBar().get(0));
		verify = co.isElementPresent(driver, dashboard.getMenuBar().get(0));
		co.Verify(verify, true);
	}

	@Test(priority = 1)
	public void creating_Voting_Period() throws InterruptedException, IOException {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		dashboard.getMenuBar().get(1).click();
		contest.getTxtSearchContest().sendKeys(prop.getProperty("ContestName"));
		Thread.sleep(4000);
		contest.getActionButtons().get(1).click();
		for (int i = 0; i < contest.getManageContestTabNameList().size(); i++) {
			if(contest.getManageContestTabNameList().get(i).getText().contains("Entries")) {
				contest.getManageContestTabNameList().get(i).click();
			}
		}
		for (int i = 0; i < contest.getManageContestLeftMenuNameList().size(); i++) {
			if(contest.getManageContestLeftMenuNameList().get(i).getText().contains("Add Entry")) {
				contest.getManageContestLeftMenuNameList().get(i).click();
			}
		}
		contest.getBtnSelectFromFile().click();
		Thread.sleep(4000);
		Runtime.getRuntime().exec("D:\\Media\\AutoIt\\fileupload.exe");
		Thread.sleep(4000);
		wait30.until(ExpectedConditions.visibilityOf(contest.getLblUploadSuccess()));
		contest.getLblChooseUser().click();
		contest.getTxtChooseUser().sendKeys(prop.getProperty("EntryUser"));
		Thread.sleep(2000);
		contest.getAutoSuggOptUserList().get(0).click();
		contest.getTxtEnrtyTitle().click();
		contest.getTxtEnrtyTitle().sendKeys(prop.getProperty("EntryTitle"));
		contest.getTxtEnrtyDesc().sendKeys(prop.getProperty("EntryDesc"));
		contest.getTxtEnrtyEmail().sendKeys(prop.getProperty("EntryUser"));
		contest.CbTermConditions.click();
		contest.getBtnSaveEntry().click();
		wait30.until(ExpectedConditions.visibilityOf(contest.LabelToastMessageTitle));
		String toastmessage = contest.LabelToastMessageTitle.getText();
		System.out.println("this is " + toastmessage);
		if(toastmessage.contains("Success")){
			verify = true;
		}
		Assert.assertEquals(true, verify);
	}
	
	@Test(priority = 2)
	public void addForm() throws Exception {
		for (int i = 0; i < contest.getManageContestTabNameList().size(); i++) {
			if(contest.getManageContestTabNameList().get(i).getText().contains("Settings")) {
				contest.getManageContestTabNameList().get(i).click();
			}
		}
		for (int i = 0; i < contest.getManageContestLeftMenuNameList().size(); i++) {
			if(contest.getManageContestLeftMenuNameList().get(i).getText().contains("Forms")) {
				contest.getManageContestLeftMenuNameList().get(i).click();
			}
		}
		contest.getBtnAddForm().click();
		contest.getBtnAddForm_Type().get(1).click();
		wait30.until(ExpectedConditions.visibilityOf(contest.LabelToastMessageTitle));
		String toastmessage = contest.LabelToastMessageTitle.getText();
		System.out.println("this is " + toastmessage);
		if(toastmessage.contains("Success")){
			verify = true;
		}
		Assert.assertEquals(true, verify);
		voteperiod.initialise();
		voteperiod.creating_Voting_Period();
		voteperiod.afterexecution();
		Thread.sleep(4000);
		dashboard.getMenuBar().get(1).click();
		/*
		 * contest.getTxtSearchContest().sendKeys(prop.getProperty("ContestName"));
		 * Thread.sleep(4000);
		 */
		//Here
		contest.getActionButtons().get(0).click();
		
		String url = contest.getLblTemplateUrl().getText();
		String cont_url = contest.getLblTemplateContestUrl().getAttribute("value");
		System.out.println("this is cont url: " +  cont_url);
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("window.open()");
		String[] win = co.handleWindows(driver);
		Set<String> window = driver.getWindowHandles();
		Iterator<String> windowset = window.iterator();
		backend = windowset.next();
		Frontend = windowset.next();
		driver.switchTo().window(Frontend);
		driver.get(url + cont_url);
		wait30.until(ExpectedConditions.visibilityOf(frontend.getTitleFrontend()));
		for(int i = 0; i < frontend.getBtnTopMenuList().size(); i++) {
			if(frontend.getBtnTopMenuList().get(i).getText().contains("Gallery")) {
				frontend.getBtnTopMenuList().get(i).click();
			}
		}
		for(int i = 0; i < frontend.getGalleryEntryNameList().size(); i++) {
			if(frontend.getGalleryEntryNameList().get(i).getText().contains("Test Entry")) {
				frontend.getGalleryEntryList().get(i).click();
			}
		}
		Thread.sleep(5000);
		
		
	}
	
	
	
	
	@AfterTest()
	public void afterexecution() {
		driver.close();
		driver.quit();
	}
	
}
