package test2;

import java.io.FileInputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import LaunchpadObjects.Contest_Objects;
import LaunchpadObjects.Dashboard_Objects;
import LaunchpadObjects.Login_Objects;
import LaunchpadObjects.Users_And_Groups_Objects;
import Objects.BaseClass;
import Objects.CommonOperations;

public class Delete_a_Contest extends BaseClass{
	
	public WebDriver driver;
	Login_Objects login;
	Dashboard_Objects dashboard;
	Contest_Objects contest;
	CommonOperations co;
	Properties prop;
	boolean verify = false;
	int PreCount = 0;
	String backend, Frontend = null;
	
	@BeforeTest
	public void initialise() throws Exception {
		driver = setup();
		login = new Login_Objects(driver);
		dashboard = new Dashboard_Objects(driver);
		contest = new Contest_Objects(driver);
		co = new CommonOperations(driver);
		prop = new Properties();
		FileInputStream fis = new FileInputStream("C:\\Users\\ABI\\Test_workspace\\TestriqDemo\\src\\main\\java\\Resources\\TestData.properties");	//To read from file
		prop.load(fis);										//to load the data form the file
		driver.get(prop.getProperty("Url"));
		login.getTxtUsername().sendKeys(prop.getProperty("UserName"));
		login.getTxtPassword().sendKeys(prop.getProperty("Password"));
		login.getBtnLogin().click();
		co.explicitlyWaitForVisibilityOfElement(driver, dashboard.getMenuBar().get(0));
		verify = co.isElementPresent(driver, dashboard.getMenuBar().get(0));
		co.Verify(verify, true);
	}
	
	@Test
	public void deleting_a_Contest() throws InterruptedException {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		dashboard.getMenuBar().get(1).click();
		contest.getTxtSearchContest().sendKeys(prop.getProperty("ContestName"));
		Thread.sleep(4000);
		contest.getActionButtons().get(3).click();
		contest.getTxtDeleteContest().sendKeys("DELETE");
		Thread.sleep(4000);
		contest.getBtnConfirmDeleteContest().click();
		Thread.sleep(5000);
		dashboard.getMenuBar().get(1).click();
		contest.getTxtSearchContest().sendKeys(prop.getProperty("ContestName"));
		Thread.sleep(4000);
		if(contest.getLblGridNoRecordsFound().isDisplayed()) {
			Assert.assertTrue(true);
		}
	}
	
	@AfterTest()
	public void afterexecution() {
		driver.close();
		driver.quit();
	}
}
