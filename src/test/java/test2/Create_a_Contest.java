package test2;

import java.io.FileInputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import LaunchpadObjects.Contest_Objects;
import LaunchpadObjects.Dashboard_Objects;
import LaunchpadObjects.Login_Objects;
import Objects.BaseClass;
import Objects.CommonOperations;

public class Create_a_Contest extends BaseClass{
	
	public WebDriver driver;
	Login_Objects login;
	Dashboard_Objects dashboard;
	Contest_Objects contest;
	CommonOperations co;
	Properties prop;
	boolean verify = false;
	int PreCount = 0;
	String backend, Frontend = null;
	
	@BeforeTest
	public void initialise() throws Exception {
		driver = setup();
		login = new Login_Objects(driver);
		dashboard = new Dashboard_Objects(driver);
		contest = new Contest_Objects(driver);
		co = new CommonOperations(driver);
		prop = new Properties();
		FileInputStream fis = new FileInputStream("C:\\Users\\ABI\\Test_workspace\\TestriqDemo\\src\\main\\java\\Resources\\TestData.properties");	//To read from file
		prop.load(fis);										//to load the data form the file
		driver.get(prop.getProperty("Url"));
		login.getTxtUsername().sendKeys(prop.getProperty("UserName"));
		login.getTxtPassword().sendKeys(prop.getProperty("Password"));
		login.getBtnLogin().click();
		co.explicitlyWaitForVisibilityOfElement(driver, dashboard.getMenuBar().get(0));
		verify = co.isElementPresent(driver, dashboard.getMenuBar().get(0));
		co.Verify(verify, true);
	}
	
	@Test
	public void creating_a_Contest() throws InterruptedException {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		dashboard.getMenuBar().get(1).click();
		contest.getBtnCreateNewContest().click();
		contest.getTxtContestname().sendKeys(prop.getProperty("ContestName"));
		contest.getTxtContestDescription().sendKeys("ContestDesc");
		contest.getBtnSaveContest().click();
		Thread.sleep(5000);
		contest.getTxtSearchContest().sendKeys(prop.getProperty("ContestName"));
		Thread.sleep(4000);
		//contest.getActionButtons().get(1).click();
		String actualContest = contest.getGridRow1ContestName().getText();
		Assert.assertEquals(actualContest, prop.getProperty("ContestName"));
		contest.getActionButtons().get(1).click();
		verify = contest.getManageContestTabNameList().get(0).isDisplayed();
		Assert.assertTrue(verify);
		for(int i = 0; i < contest.getManageContestTabNameList().size(); i++ ) {
			if(contest.getManageContestTabNameList().get(i).getText().contains("Settings")) {
				contest.getManageContestTabNameList().get(i).click();
			}
		}
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,250)");
		Select s = new Select(contest.getDdContestStatus());
		s.selectByVisibleText("Published");
		js.executeScript("window.scrollBy(0,700)");
		contest.getBtnSettingGeneralSave().click();
		dashboard.getMenuBar().get(1).click();
		Thread.sleep(4000);
		String actualStatus = contest.getGridRow1ContestStatus().getText();
		Assert.assertEquals(actualStatus, "Published");
	}
	
	@AfterTest()
	public void afterexecution() {
		driver.close();
		driver.quit();
	}

}
