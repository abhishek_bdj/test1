package test2;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import LaunchpadObjects.Contest_Objects;
import LaunchpadObjects.Dashboard_Objects;
import LaunchpadObjects.Login_Objects;
import Objects.BaseClass;
import Objects.CommonOperations;

public class Set_DefaultTemplate extends BaseClass{
	
	public WebDriver driver;
	Login_Objects login;
	Dashboard_Objects dashboard;
	Contest_Objects contest;
	CommonOperations co;
	Properties prop;
	boolean verify = false;
	int PreCount = 0;
	String backend, Frontend = null;
	
	@BeforeTest
	public void initialise() throws IOException {
		driver = setup();
		login = new Login_Objects(driver);
		dashboard = new Dashboard_Objects(driver);
		contest = new Contest_Objects(driver);
		co = new CommonOperations(driver);
		prop = new Properties();
		FileInputStream fis = new FileInputStream("C:\\Users\\ABI\\Test_workspace\\TestriqDemo\\src\\main\\java\\Resources\\TestData.properties");	//To read from file
		prop.load(fis);		
		driver.get(prop.getProperty("Url"));
		login.getTxtUsername().sendKeys(prop.getProperty("UserName"));
		login.getTxtPassword().sendKeys(prop.getProperty("Password"));
		login.getBtnLogin().click();
		co.explicitlyWaitForVisibilityOfElement(driver, dashboard.getMenuBar().get(0));
		verify = co.isElementPresent(driver, dashboard.getMenuBar().get(0));
		co.Verify(verify, true);
	}
	
	
	@Test(priority = 1)
	public void set_Template_As_Default() throws InterruptedException {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		dashboard.getMenuBar().get(1).click();
		contest.getTxtSearchContest().sendKeys(prop.getProperty("ContestName"));
		Thread.sleep(4000);
		contest.getActionButtons().get(0).click();
		PreCount = contest.getSavedTemplatesList().size();
		for (int i = 0; i < contest.getBtnDesignTopTabList().size(); i++) {
			if(contest.getBtnDesignTopTabList().get(i).getText().contains("Template Library")) {
				contest.getBtnDesignTopTabList().get(i).click();
			}
		}
		for(int i = 0; i < contest.getLibraryTemplatesTitleList().size(); i++) {
			if(contest.getLibraryTemplatesTitleList().get(i).getText().contains(prop.getProperty("TemplateTitle"))){
				contest.getBtnEditTemplatesList().get(i).click();
			}
			Thread.sleep(5000);
		}
		contest.getBtnTemplateSave().click();
		for (int i = 0; i < contest.getBtnDesignTopTabList().size(); i++) {
			if(contest.getBtnDesignTopTabList().get(i).getText().contains("My Saved Templates")) {
				contest.getBtnDesignTopTabList().get(i).click();
			}
		}
		if(PreCount < contest.getSavedTemplatesList().size()) {
			Assert.assertTrue(true);
		}
		for(int i = 0; i < contest.getSavedTemplatesTitleList().size(); i++) {
			if(contest.getSavedTemplatesTitleList().get(i).getText().contains(prop.getProperty("TemplateTitle"))){
				/*
				 * contest.getBtnActiveSavedTemplatesList().get(i).click(); Thread.sleep(5000);
				 */
				contest.getCbDefaultSavedTemplatesList().get(i).click();
			}
		}
		String url = contest.getLblTemplateUrl().getText();
		String cont_url = contest.getLblTemplateContestUrl().getAttribute("value");
		System.out.println("this is cont url: " +  cont_url);
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("window.open()");
		String[] win = co.handleWindows(driver);
		Set<String> window = driver.getWindowHandles();
		Iterator<String> windowset = window.iterator();
		backend = windowset.next();
		Frontend = windowset.next();
		driver.switchTo().window(Frontend);
		driver.get(url + cont_url);
		Thread.sleep(5000);
		if(contest.getContentTitleNewTemplateTab().getText().contains(prop.getProperty("NewTabContent"))) {
			Assert.assertTrue(true);
		}
	}
	
	@AfterTest()
	public void afterexecution() {
		driver.close();
		driver.quit();
	}

}
