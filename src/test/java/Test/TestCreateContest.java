package Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import Objects.BaseClass;
import Objects.CommonOperations;
import Objects.ContestObjects;
import Objects.DashboardObjects;
import Objects.LoginObjects;

public class TestCreateContest extends BaseClass{
	
	public WebDriver driver;
	public LoginObjects login;
	public DashboardObjects dashboard;
	public ContestObjects contest;
	public CommonOperations co;
	public Properties prop;
	public WebDriverWait wait30;
	public boolean verify = false;
	public String ContestName, ContestDesc, Label, Placeholder = null; 
	
	@BeforeTest
	public void setupTest() throws IOException {
		//System.setProperty("webdriver.chrome.driver", "D:\\AutomationDrivers\\ChromeDriver\\94.0.4606.61\\chromedriver.exe");
		//driver = new ChromeDriver();
		//driver.manage().window().maximize();
		//driver = setup();
		driver.get("https://manager.uat.launchpad6.com/login?return=/mysite");
		wait30 = new WebDriverWait(driver, 30);
		prop = new Properties();
		FileInputStream fis = new FileInputStream("C:\\Users\\ABI\\Test_workspace\\TestriqDemo\\src\\main\\java\\Resources\\TestData.properties");	//To read from file
		prop.load(fis);										//to load the data form the file
		
	}
	
	@Test(priority=1)  
	public void loginLaunchpad() throws InterruptedException {
		//login = new LoginObjects(driver);
		LoginObjects login = new LoginObjects(driver);
		login.login();
		dashboard = new DashboardObjects(driver);
		wait30.until(ExpectedConditions.visibilityOf(dashboard.MenuBar.get(0)));
		boolean verify = dashboard.MenuBar.get(0).isDisplayed();
		Assert.assertTrue(verify);
		//dashboard.SelectMenuBar("Contests");
	}
	
	@Test(priority=2)
	public void navigateToContestScreen() {
		dashboard = new DashboardObjects(driver);
		dashboard.selectMainMenuBar("Contests");
		contest = new ContestObjects(driver);
		contest.verifyContestPageVisible("ContestPage");
	}
	
	@Test(priority=3)  
	public void createNewContest() throws InterruptedException {
		contest = new ContestObjects(driver);
		contest.clickCreateContestBtn();
		Thread.sleep(5000);
		contest.verifyContestPageVisible("CreateContestPopup");
		ContestName = prop.getProperty("ContestName");
		ContestDesc = prop.getProperty("ContestDesc");
		contest.enterContestDetails(ContestName,ContestDesc);
		contest.clickSaveContestBtn();
		contest.VerifyContestSaved(ContestName);
	}
	
	@Test(priority=4)
	public void addForm() throws InterruptedException {
		contest.clickonAction("Manage");
		co = new CommonOperations(driver);
		verify = co.isElementPresent(driver, contest.ManageContestTabNameList.get(0));
		co.Verify(verify, true);
		contest.clickManageContestTabName("Settings");
		verify = co.isElementPresent(driver, contest.ManageContestTabNameList.get(0));
		co.Verify(verify, true);
		co = new CommonOperations(driver);
		contest.ContestStatus.click();
		co.selectFromDropdownList(driver, contest.ContestStatus, "Published");
		co.scrollDownTillElement(driver, contest.BtnSettingGeneralSave);
		contest.clickOnBtnSettingGeneralSave();
		co.verifyToastMessage("Successfully");
		//wait30.until(ExpectedConditions.visibilityOf(contest.BtnAddForm));
		contest.clickManageContestLeftMenuName("Forms");
		contest.clickAddForm();
		contest.clickAddFormType("Vote");
		co.verifyToastMessage("Successfully");
	}
	
	@Test(priority=5, dependsOnMethods = {"addForm"} )
	public void addVoteFormFields() {
		contest.clickAddFormFields();
		Label = "Label " + CommonOperations.getAlphaNumericString(5);
		Placeholder = "Placeholder " + CommonOperations.getAlphaNumericString(10);
		contest.enterVoteFormDetails(Label, Placeholder, "50", "100");
		contest.clickSaveFormFields();
		co.verifyToastMessage("Successfully");
	}
	
	/*
	 * @Test(priority=6) public void deleteContest() throws InterruptedException {
	 * dashboard.selectMainMenuBar("Contests"); contest = new
	 * ContestObjects(driver); contest.verifyContestPageVisible("ContestPage");
	 * contest.enterSearchData(ContestName); Thread.sleep(5000);
	 * contest.clickonAction("Delete"); co = new CommonOperations(driver);
	 * wait30.until(ExpectedConditions.visibilityOf(contest.
	 * DeleteContestConfirmPopup)); verify = co.isElementPresent(driver,
	 * contest.DeleteContestConfirmPopup); co.Verify(verify, true);
	 * contest.enterConfirmDeleteText(); contest.clickOnConfirmDeleteContestBtn();
	 * co.verifyToastMessage("Successfully"); }
	 */
	
	@AfterTest
	public void tearDownTest() {
		tearDown();
	}
}
