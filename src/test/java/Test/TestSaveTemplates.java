package Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Objects.BaseClass;
import Objects.CommonOperations;
import Objects.ContestObjects;
import Objects.DashboardObjects;
import Objects.LoginObjects;

public class TestSaveTemplates extends BaseClass{
	
	public WebDriver driver;
	//public LoginObjects login;
	public DashboardObjects dashboard;
	public ContestObjects contest;
	public CommonOperations co;
	public Properties prop;
	public WebDriverWait wait30;
	public boolean verify = false;
	public int PreCount = 0;
	
	@BeforeTest
	public void setupTest() throws IOException {
		//System.setProperty("webdriver.chrome.driver", "D:\\AutomationDrivers\\ChromeDriver\\94.0.4606.61\\chromedriver.exe");
		//driver = new ChromeDriver();
		//driver.manage().window().maximize();
		driver = setup();
		driver.get("https://manager.uat.launchpad6.com/login?return=/mysite");
		wait30 = new WebDriverWait(driver, 30);
		prop = new Properties();
		FileInputStream fis = new FileInputStream("C:\\Users\\ABI\\Test_workspace\\TestriqDemo\\src\\main\\java\\Resources\\TestData.properties");	//To read from file
		prop.load(fis);										//to load the data form the file
	}
	
	@AfterTest
	public void tearDownTest() {
		tearDown();
	}
	
	@Test(priority=1)  
	public void loginLaunchpad() throws InterruptedException {
		//login = new LoginObjects(driver);
		LoginObjects login = new LoginObjects(driver);
		login.login();
		dashboard = new DashboardObjects(driver);
		wait30.until(ExpectedConditions.visibilityOf(dashboard.MenuBar.get(0)));
		boolean verify = dashboard.MenuBar.get(0).isDisplayed();
		Assert.assertTrue(verify);
		//dashboard.SelectMenuBar("Contests");
	}
	
	@Test(priority=2)
	public void navigateToContestScreen() {
		dashboard = new DashboardObjects(driver);
		dashboard.selectMainMenuBar("Contests");
		contest = new ContestObjects(driver);
		contest.verifyContestPageVisible("ContestPage");
	}
	
	@Test(priority=3)
	public void copyTemplate() throws Exception {
		contest.searchContest(prop.getProperty("ContestName"));
		contest.clickonAction("Design");
		co = new CommonOperations(driver);
		wait30.until(ExpectedConditions.visibilityOf(contest.BtnDesignTopTabList.get(0)));
		verify = co.isElementPresent(driver, contest.BtnDesignTopTabList.get(0));
		co.Verify(verify, true);
		PreCount = contest.SavedTemplatesList.size();
		contest.clickDesignScreenTab("Template Library");
		wait30.until(ExpectedConditions.visibilityOf(contest.BtnEditTemplatesList.get(0)));
		verify = co.isElementPresent(driver, contest.BtnEditTemplatesList.get(0));
		co.Verify(verify, true);
		contest.clickOnEditTemplate(1);
		wait30.until(ExpectedConditions.visibilityOf(contest.BtnTemplateSave));
		verify = co.isElementPresent(driver, contest.BtnTemplateSave);
		co.Verify(verify, true);
		contest.clickOnTemplateSaveBtn();
		co.verifyToastMessage2("Success");
		contest.clickDesignScreenTab("My Saved Templates");
		verify = contest.verifySavedTemplatesCount(PreCount);
		co.Verify(verify, true);
	}
	
	
	
	
	
	

}
