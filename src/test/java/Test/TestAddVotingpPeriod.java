package Test;

import java.io.FileInputStream;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Objects.BaseClass;
import Objects.CommonOperations;
import Objects.ContestObjects;
import Objects.DashboardObjects;
import Objects.LoginObjects;

public class TestAddVotingpPeriod extends BaseClass{
	
	public WebDriver driver;
	//public LoginObjects login;
	public DashboardObjects dashboard;
	public ContestObjects contest;
	public CommonOperations co;
	public Properties prop;
	public WebDriverWait wait30;
	public boolean verify = false;

	
	@BeforeTest
	public void setupTest() throws Exception {
		//System.setProperty("webdriver.chrome.driver", "D:\\AutomationDrivers\\ChromeDriver\\94.0.4606.61\\chromedriver.exe");
		//driver = new ChromeDriver();
		//driver.manage().window().maximize();
		driver = setup();
		driver.get("https://manager.uat.launchpad6.com/login?return=/mysite");
		wait30 = new WebDriverWait(driver, 30);
		prop = new Properties();
		co = new CommonOperations(driver);
		FileInputStream fis = new FileInputStream("C:\\Users\\ABI\\Test_workspace\\TestriqDemo\\src\\main\\java\\Resources\\TestData.properties");	//To read from file
		prop.load(fis);										//to load the data form the file
	}
	
	@AfterTest
	public void tearDownTest() {
		tearDown();
	}
	
	@Test(priority=1)  
	public void loginLaunchpad() throws InterruptedException {
		//login = new LoginObjects(driver);
		LoginObjects login = new LoginObjects(driver);
		login.login();
		dashboard = new DashboardObjects(driver);
		wait30.until(ExpectedConditions.visibilityOf(dashboard.MenuBar.get(0)));
		boolean verify = dashboard.MenuBar.get(0).isDisplayed();
		Assert.assertTrue(verify);
		//dashboard.SelectMenuBar("Contests");
	}
	
	@Test(priority=2)
	public void navigateToContestScreen() {
		dashboard = new DashboardObjects(driver);
		dashboard.selectMainMenuBar("Contests");
		contest = new ContestObjects(driver);
		contest.verifyContestPageVisible("ContestPage");
	}
	
	@Test(priority=3)
	public void addVotingPeriod() throws Exception {
		contest.searchContest(prop.getProperty("ContestName"));
		Thread.sleep(5000);
		contest.clickonAction("Manage");
		co.explicitlyWaitForVisibilityOfElement(driver, contest.ManageContestTabNameList.get(0));
		contest.clickManageContestTabName("Settings");
		co.explicitlyWaitForVisibilityOfElement(driver, contest.ManageContestLeftMenuNameList.get(0));
		contest.clickManageContestLeftMenuName("Voting & Judging");
		co.explicitlyWaitForVisibilityOfElement(driver, contest.BtnAddPeriod);
		contest.clickOnAddPeriod();
		contest.clickAddPeriodType("Voting Period");
		wait30.until(ExpectedConditions.visibilityOf(contest.TxtVotingPeriodName));
		verify = co.isElementPresent(driver, contest.TxtVotingPeriodName);
		co.Verify(verify, true);
		contest.addVotingPeriodDetails(prop.getProperty("VotingPeriodName"), prop.getProperty("VoteStartDate"), prop.getProperty("VoteEndDate"), prop.getProperty("VotingLimitPer"), prop.getProperty("VoteEntryEligibility"));
		co.verifyToastMessage("Successfully");
	}
	
	
	
	
}