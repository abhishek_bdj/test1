package Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import Objects.BaseClass;
import Objects.CommonOperations;
import Objects.ContestObjects;
import Objects.DashboardObjects;
import Objects.LoginObjects;

public class TestSetDefaultTemplate extends BaseClass {

	public WebDriver driver;
	// public LoginObjects login;
	public DashboardObjects dashboard;
	public ContestObjects contest;
	public CommonOperations co;
	public Properties prop;
	public WebDriverWait wait30;
	public boolean verify = false;
	public int PreCount = 0;
	public String backend, Frontend = null;

	@BeforeTest
	public void setupTest() throws IOException {
		// System.setProperty("webdriver.chrome.driver",
		// "D:\\AutomationDrivers\\ChromeDriver\\94.0.4606.61\\chromedriver.exe");
		// driver = new ChromeDriver();
		// driver.manage().window().maximize();
		driver = setup();
		driver.get("https://manager.uat.launchpad6.com/login?return=/mysite");
		wait30 = new WebDriverWait(driver, 30);
		prop = new Properties();
		// To read from file
		FileInputStream fis = new FileInputStream("C:\\Users\\ABI\\Test_workspace\\TestriqDemo\\src\\main\\java\\Resources\\TestData.properties"); 
		// to load the data form the file
		prop.load(fis); 
	}

	@AfterTest
	public void tearDownTest() {
		tearDown();
	}

	@Test(priority = 1)
	public void loginLaunchpad() throws InterruptedException {
		// login = new LoginObjects(driver);
		LoginObjects login = new LoginObjects(driver);
		login.login();
		dashboard = new DashboardObjects(driver);
		wait30.until(ExpectedConditions.visibilityOf(dashboard.MenuBar.get(0)));
		boolean verify = dashboard.MenuBar.get(0).isDisplayed();
		Assert.assertTrue(verify);
		// dashboard.SelectMenuBar("Contests");
	}

	@Test(priority = 2)
	public void navigateToContestScreen() {
		dashboard = new DashboardObjects(driver);
		dashboard.selectMainMenuBar("Contests");
		contest = new ContestObjects(driver);
		contest.verifyContestPageVisible("ContestPage");
	}

	@Test(priority = 3)
	public void setDefaultTemplate() throws Exception {
		contest.searchContest(prop.getProperty("ContestName"));
		contest.clickonAction("Design");
		co = new CommonOperations(driver);
		co.explicitlyWaitForVisibilityOfElement(driver, contest.BtnDesignTopTabList.get(0));
		contest.clickDesignScreenTab("Template Library");
		co.explicitlyWaitForVisibilityOfElement(driver, contest.BtnEditTemplatesList.get(0));
		contest.selectParticularTemplate(prop.getProperty("TemplateTitle"));
		co.explicitlyWaitForVisibilityOfElement(driver, contest.BtnTemplateSave);
		contest.clickOnTemplateSaveBtn();
		co.verifyToastMessage2("Success");
		contest.clickDesignScreenTab("My Saved Templates");
		contest.setDefaultSavedTemplate(prop.getProperty("TemplateTitle"));
		co.verifyToastMessage2("Success");
	}

	@Test(priority = 4)
	public void opnInNewTab() throws InterruptedException {
		String url = contest.concatUrl();
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("window.open()");
		String[] win = co.handleWindows(driver);
		Set<String> window = driver.getWindowHandles();
		Iterator<String> windowset = window.iterator();
		backend = windowset.next();
		Frontend = windowset.next();
		driver.switchTo().window(Frontend);
		driver.get(url);
		Thread.sleep(5000);
		contest.verifyNewTabContentTitle(prop.getProperty("NewTabContent"));
		driver.switchTo().window(backend);
	}
	
	@Test(priority = 5)
	public void deleteAddedTemplate() throws InterruptedException {
		contest.resetDefaultSavedTemplate("New Space 1");
		contest.deleteSavedTemplate(prop.getProperty("TemplateTitle"));
		co = new CommonOperations(driver);
		co.verifyToastMessage2("Success");
	}

}
