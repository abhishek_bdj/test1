package Test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Objects.BaseClass;
import Objects.CommonOperations;
import Objects.ContestObjects;
import Objects.DashboardObjects;
import Objects.LoginObjects;
import Objects.UsersGroupsObjects;

public class TestCreateGroup extends BaseClass {
	
	public WebDriver driver;
	public WebDriverWait wait30;
	public boolean verify = false;
	public Properties prop;
	public DashboardObjects dashboard;
	public ContestObjects contest;
	public UsersGroupsObjects users_groups;
	
	@BeforeTest
	public void setupTest() throws Exception {
		/*
		 * System.setProperty("webdriver.chrome.driver",
		 * "D:\\AutomationDrivers\\ChromeDriver\\94.0.4606.61\\chromedriver.exe");
		 * driver = new ChromeDriver(); driver.manage().window().maximize();
		 */
		driver = setup();
		driver.get("https://manager.uat.launchpad6.com/login?return=/mysite");
		wait30 = new WebDriverWait(driver, 30);
		prop = new Properties();
		FileInputStream fis = new FileInputStream("C:\\Users\\ABI\\Test_workspace\\TestriqDemo\\src\\main\\java\\Resources\\TestData.properties");	//To read from file
		prop.load(fis);										//to load the data form the file
	}
	
	@Test(priority=1)  
	public void loginLaunchpad() throws InterruptedException {
		//login = new LoginObjects(driver);
		LoginObjects login = new LoginObjects(driver);
		login.login();
		dashboard = new DashboardObjects(driver);
		wait30.until(ExpectedConditions.visibilityOf(dashboard.MenuBar.get(0)));
		boolean verify = dashboard.MenuBar.get(0).isDisplayed();
		Assert.assertTrue(verify);
		//dashboard.SelectMenuBar("Contests");
	}
	
	@Test(priority=2)
	public void navigateToUsersGroupsScreen() {
		dashboard = new DashboardObjects(driver);
		dashboard.selectMainMenuBar("User & Groups");
		users_groups = new UsersGroupsObjects(driver);
		users_groups.verifyUserPageVisible("User");
	}
	
	@Test(priority=3)
	public void createNewGroup() throws Exception {
		users_groups.clickUserTopTabName("Groups");
		users_groups.addNewGroup(prop.getProperty("GroupName"));
		//wait30.until(ExpectedConditions.visibilityOf(users_groups.BtnTools));
		users_groups.addUsersInGroup(prop.getProperty("UserEmail"));
	}
	
	@AfterTest
	public void tearDownTest() {
		tearDown();
	}
	
	
	
}
