/*
package Contest;

import java.io.IOException;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Launchpad6_objects.Category_Object;
import Launchpad6_objects.Comment_objects;
import Launchpad6_objects.Contest_Object;
import Launchpad6_objects.Contest_Objects;
import Launchpad6_objects.Entries_Object;
import Launchpad6_objects.Frontend_Objects;
import Launchpad6_objects.Login_object;
import Launchpad6_objects.Users_Object;
import Launchpad6_objects.Votes_Object;
import resources.Baseclassobjects;

public class Create_a_Contest extends Baseclassobjects {

	public WebDriver driver;
	Users_Object ub;
	Login_object lo;
	Contest_Object co;
	Category_Object cato;
	String vote;
	String Frontendurl;
	Frontend_Objects fo;
	Entries_Object entryobj;
	Set<String> window;
	Votes_Object votes;
	WebDriver newdriver;
	Comment_objects comments;
	Votes Vo;
	Contest_Objects contest;

	@BeforeTest()
	public void initialise() throws IOException {
		driver = baseelements();
		driver.get(pro.getProperty("UATurl"));
		lo = new Login_object(driver);
		ub = new Users_Object(driver);
		co = new Contest_Object(driver);
		fo = new Frontend_Objects(driver);
		cato = new Category_Object(driver);
		votes = new Votes_Object(driver);
		comments = new Comment_objects(driver);
		entryobj = new Entries_Object(driver);
		contest= new Contest_Objects(driver);
		lo.getUsername().sendKeys(pro.getProperty("username"));
		lo.getPassword().sendKeys(pro.getProperty("password"));
		lo.getLogin().click();

	}

// Creating A contest
	@Test(priority = 1)
	public void creating_a_contest_4735() throws InterruptedException {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		co.getContestmenu().click();
		co.getCreateanewcontest().click();
		co.getContestname().sendKeys(pro.getProperty("newcontest"));
		String actualdescription = pro.getProperty("newcontest");
		co.getContestdescription().sendKeys(pro.getProperty("contestdescription"));
		co.getCreate().click();
		co.getSearchcontest().sendKeys(pro.getProperty("newcontest"));
		Thread.sleep(4000);
		String contestdescription = co.getContestdescription1().getText();
		Assert.assertEquals(actualdescription, contestdescription);
		co.getManagecontestunique().click();
		for (int i = 0; i < co.getContestmanagelist().size(); i++) {
			if (co.getContestmanagelist().get(i).getText().contains("Settings")) {
				co.getContestmanagelist().get(i).click();
			}
		}
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,250)");
		Select s = new Select(co.getStatusdropdown());
		s.selectByVisibleText("Published");
		js.executeScript("window.scrollBy(0,700)");
		comments.getPostingCommentToggle().click();
		co.getContestsettingsave().click();
		co.getContestmenu().click();
		Assert.assertEquals(co.getConteststatuscheck().getText(), "Published");

	}

// Adding template in Design
//	@Test(priority = 2)
	public void adddesignButton_4863_4737() {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		co.getManagecontestunique().click();
		co.getUniquedesign().click();
		for (int i = 0; i < co.getDesignmenus().size(); i++) {
			if (co.getDesignmenus().get(i).getText().contains("My Saved")) {
				co.getDesignmenus().get(i).click();
				break;
			}
		}
		Assert.assertEquals(co.getNosavedtemplate().getText(), "No saved templates found.");
		for (int j = 0; j < co.getDesignmenus().size(); j++) {
			if (co.getDesignmenus().get(j).getText().contains("Template Library")) {
				co.getDesignmenus().get(j).click();
				break;
			}
		}
		co.getTemplatelibraryedu().click();
		co.getTempaltelibrsave().click();
		for (int k = 0; k < co.getDesignmenus().size(); k++) {
			if (co.getDesignmenus().get(k).getText().contains("My Saved")) {
				co.getDesignmenus().get(k).click();
				break;
			}
		}
		Assert.assertTrue(co.getConfirmdesigncode().isDisplayed());
		co.getContestmenu().click();
	}

	// Searching the contest
//	@Test(priority = 3)
	public void searchFunctionalityinrecords_5222() throws InterruptedException {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		co.getContestmenu().click();
		co.getRecorddropdown().click();
		for (int i = 0; i < co.getRecordlist().size(); i++) {
			if (co.getRecordlist().get(i).getText().contains("10")) {
				co.getRecordlist().get(i).click();
				break;
			}

		}
		Thread.sleep(3000);
		int count = co.getIdcount().size();
		Assert.assertTrue(count <= 10);
		co.getContestmenu().click();
		co.getRecorddropdown().click();
		for (int j = 0; j < co.getRecordlist().size(); j++) {
			if (co.getRecordlist().get(j).getText().contains("25")) {
				co.getRecordlist().get(j).click();
				break;
			}

		}
		Thread.sleep(3000);
		int count2 = co.getIdcount().size();
		Assert.assertTrue(count2 <= 25);
	}

	// Adding removing the column in contest
//	@Test(priority = 4)
	public void Column_search_record_4736() {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		co.getContestmenu().click();
		co.getColumns().click();
		for (int i = 0; i < co.getColumnlist().size(); i++) {
			if (co.getColumnlist().get(i).getText().contains("Id")) {
				co.getColumnlist().get(i).click();
			}
		}
		int a = co.getRowlist().size();
		Assert.assertEquals(a, 7);
		co.getContestmenu().click();
		co.getColumns().click();
		for (int j = 0; j < co.getColumnlist().size(); j++) {
			if (co.getColumnlist().get(j).getText().contains("Id")) {
				co.getColumnlist().get(j).click();
			}
		}
		int b = co.getRowlist().size();
		Assert.assertEquals(b, 8);
	}

	// preview the contest
	//@Test(priority = 5)
	public void preview_Template_4739() throws InterruptedException {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		co.getContestmenu().click();
		co.getDesigncontestunique().click();
		co.getPreview().click();
		Thread.sleep(2000);
		String name = fo.getFrontendmenu().get(0).getText();
		Assert.assertFalse(name.isEmpty());
		driver.navigate().back();
		co.getContestmenu();

	}

//Manage button in design section
//	@Test(priority = 6)
	public void Verify_Mange_buttonDesign_4743() throws InterruptedException {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		co.getContestmenu().click();
		Thread.sleep(2000);
		co.getDesigncontestunique().click();
		Thread.sleep(2000);
		co.getManageentryformbutton().click();
		Thread.sleep(2000);
		String overview = co.getContestmanagelist().get(0).getText();
		Assert.assertEquals(overview, "Overview");

	}
	@Test(priority = 7)
	public void SinglePageEntry_4369() throws InterruptedException
	{
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		co.getContestmenu().click();
		co.getDesigncontestunique().click();
		Thread.sleep(2000);
		for (int j = 0; j < co.getDesignmenus().size(); j++) {
			if (co.getDesignmenus().get(j).getText().contains("Template Library")) {
				co.getDesignmenus().get(j).click();
				break;
			}
		}
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,400)");
		co.getSinglePageedit().click();
		co.getTempaltelibrsave().click();
		Thread.sleep(2000);
		for (int k = 0; k < co.getDesignmenus().size(); k++) {
			if (co.getDesignmenus().get(k).getText().contains("My Saved")) {
				co.getDesignmenus().get(k).click();
				break;
			}
		}
		for (int l = 0; l < co.getDesignmenus().size(); l++) {
			if (co.getDesignmenus().get(l).getText().contains("My Saved")) {
				co.getDesignmenus().get(l).click();
				break;
			}
		}
		
	}
	

	@AfterTest()
	public void afterexecution() {
		driver.close();
		driver.quit();
	}

}
*/